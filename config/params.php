<?php

return [
    'adminEmail' => 'admin@example.com',
	'user.passwordResetTokenExpire' => 3600,
	'supportEmail' => 'robot@socialsched.com',
    'senderName' => 'Example.com mailer',
];
