### Local Deployment:
1. create DB table
2. Run ```composer update```
3. Copy .env.example to .env and set config for your server 
3. Run ```yii migrate```


#### How to show raw sql
```->createCommand()->getRawSql()```