<?php

use yii\db\Migration;

/**
 * Class m210120_083329_post
 */
class m210120_083329_post extends Migration
{
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('posts', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer()->notNull(),
			'status' => $this->string(10)->notNull(),
			'title' => $this->string(250)->notNull(),
			'body' => $this->text()->notNull(),
			'media' => $this->string(250),
			'hashtag' => $this->string(250),
			'website' => $this->string(250),
			'date_publication_start' => $this->dateTime()->notNull(),
			'date_publication_end' => $this->dateTime(),
			'created_at' => $this->dateTime()->notNull(),
			'updated_at' => $this->dateTime()->notNull(),
		], $tableOptions);
		
		$this->addForeignKey(
			'fk-post-user_id',
			'posts',
			'user_id',
			'users',
			'id',
			'CASCADE',
			'CASCADE'
		);
	}
	
	public function down()
	{
		$this->dropForeignKey('fk-post-user_id', 'posts');
		
		$this->dropTable('posts');
	}
}
