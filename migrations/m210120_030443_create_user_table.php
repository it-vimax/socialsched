<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m210120_030443_create_user_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('users', [
			'id' => $this->primaryKey(),
			'username' => $this->string()->notNull()->unique(),
			'first_name' => $this->string(32)->notNull(),
			'last_name' => $this->string(32)->notNull(),
			'birthdate' => $this->date()->notNull(),
			'gender' => $this->string(32)->notNull(),
			'auth_key' => $this->string(32)->notNull(),
			'password_hash' => $this->string()->notNull(),
			'password_reset_token' => $this->string()->unique(),
			'email' => $this->string()->notNull()->unique(),
			'status' => $this->smallInteger()->notNull()->defaultValue(10),
			'photo'=> $this->string(),
			'cover'=> $this->string(),
			'created_at' => $this->dateTime()->notNull(),
			'updated_at' => $this->dateTime()->notNull(),
		], $tableOptions);
		
		$this->insert('users', [
			'username' => 'Admin',
			'first_name' => 'AdminName',
			'last_name' => 'AdminLast',
			'birthdate' => '2000-05-21',
			'gender' =>'other',
			'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('s1cret'),
			'auth_key' => Yii::$app->security->generateRandomString(),
			'email' => 'admin@socialshed.loc',
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
		]);
	}
	
	public function down()
	{
		$this->dropTable('users');
	}
}
