<?php

use yii\db\Migration;

/**
 * Class m210203_151236_alter_status_default_user
 */
class m210203_151236_alter_status_default_user extends Migration
{

    public function up()
    {
		$this->alterColumn('users', 'status', $this->smallInteger()->notNull()->defaultValue(0));
    }

    public function down()
    {
		$this->alterColumn('users', 'status', $this->smallInteger()->notNull()->defaultValue(10));
    }
}
