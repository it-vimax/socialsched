<?php

use yii\db\Migration;

/**
 * Class m210126_102842_alter_post_body
 */
class m210126_102842_alter_table_post extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
		$this->execute('
			ALTER TABLE
				posts
				CHANGE body body
				VARCHAR(191)
				CHARACTER SET utf8mb4
				COLLATE utf8mb4_unicode_ci;
		');
    }

    public function down()
    {
        echo "m210126_102842_alter_post_body cannot be reverted.\n";

        return false;
    }
}
