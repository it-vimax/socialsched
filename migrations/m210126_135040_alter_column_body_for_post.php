<?php

use yii\db\Migration;

/**
 * Class m210126_135040_alter_colum_body_for_post
 */
class m210126_135040_alter_column_body_for_post extends Migration
{    /**
 * {@inheritdoc}
 */
	public function safeUp()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->execute('
			ALTER TABLE
				posts
				CHANGE body body
				LONGTEXT
				CHARACTER SET utf8mb4
				COLLATE utf8mb4_unicode_ci;
		');
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function safeDown()
	{
		$this->alterColumn('posts', 'body', $this->text()->notNull());
	}
}