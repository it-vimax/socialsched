<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%help_messages}}`.
 */
class m210125_105530_create_help_messages_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('help_messages', [
			'id' => $this->primaryKey(),
			'user_id' =>$this->integer(),
			'username' =>$this->string()->notNull(),
			'email' =>$this->string()->notNull(),
			'subject' =>$this->string()->notNull(),
			'body' =>$this->text()->notNull(),
		], $tableOptions);

		$this->addForeignKey(
			'fk-help_messages-user_id',
			'help_messages',
			'user_id',
			'users',
			'id',
			'SET NULL',
			'CASCADE'
		);
	}
	
	public function down()
	{
		$this->dropForeignKey('fk-help_messages-user_id', 'help_messages');
		
		$this->dropTable('help_messages');
	}
}
