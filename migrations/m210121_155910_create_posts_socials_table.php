<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%posts_socials}}`.
 */
class m210121_155910_create_posts_socials_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('posts_socials', [
			'post_id' => $this->integer()->notNull(),
			'social_id' =>$this->integer()->notNull(),
		], $tableOptions);
		
		$this->addForeignKey(
			'fk-posts_socials-post_id',
			'posts_socials',
			'post_id',
			'posts',
			'id',
			'CASCADE',
			'CASCADE'
		);
		
		$this->addForeignKey(
			'fk-posts_socials-social_id',
			'posts_socials',
			'social_id',
			'socials',
			'id',
			'CASCADE',
			'CASCADE'
		);
		
		$this->createIndex(
			'idx-post-social',
			'posts_socials',
			['post_id', 'social_id'],
			true
		);
	}
	
	public function down()
	{
		$this->dropForeignKey('fk-posts_socials-social_id', 'posts_socials');
		$this->dropForeignKey('fk-posts_socials-post_id', 'posts_socials');
		
		$this->dropTable('posts_socials');
	}
}
