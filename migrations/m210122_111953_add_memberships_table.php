<?php

use yii\db\Migration;

/**
 * Class m210122_111953_add_memberships_table
 */
class m210122_111953_add_memberships_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('memberships', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'description' => $this->text()->notNull(),
			'level' => $this->integer()->notNull(),
			'platforms_count' => $this->integer()->notNull(),
			'publications_count' => $this->integer()->notNull(),
			'price' => $this->float()->notNull(),
		], $tableOptions);
		
		$this->batchInsert('memberships',['name', 'description', 'level', 'platforms_count', 'publications_count', 'price',], [
			[
				'name' => 'Membersip level 2',
				'description' => 'Allows Twitter Instagram Facebook 1 platform 15 posts',
				'level' => 2,
				'platforms_count' => 1,
				'publications_count' => 15,
				'price' => 4.99,
			],
			[
				'name' => 'Membersip level 3',
				'description' => 'Includes posts to Twitter Instagram Facebook (if included)',
				'level' => 3,
				'platforms_count' => 3,
				'publications_count' => 30,
				'price' => 9.99,
			],
			[
				'name' => 'Membersip level 4',
				'description' => 'Includes posts to Twitter Instagram Facebook (if included)',
				'level' => 4,
				'platforms_count' => -1,
				'publications_count' => 50,
				'price' => 19.99,
			],
		]);
		
		$this->addColumn('users', 'membership_id', $this->integer());
		$this->addColumn('users', 'publications_count', $this->integer()->notNull()->defaultValue(0));
		
		$this->addForeignKey(
			'fk-users-membership_id',
			'users',
			'membership_id',
			'memberships',
			'id',
			'SET NULL',
			'CASCADE'
		);
	}
	
	public function down()
	{
		$this->dropForeignKey('fk-users-membership_id', 'users');
		
		$this->dropColumn('users', 'membership_id');
		$this->dropColumn('users', 'publications_count');
		
		$this->dropTable('memberships');
	}
}
