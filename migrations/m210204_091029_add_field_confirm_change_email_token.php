<?php

use yii\db\Migration;

/**
 * Class m210204_091029_add_fiedl_confirm_change_email_token
 */
class m210204_091029_add_field_confirm_change_email_token extends Migration
{
	public function up()
	{
		$this->addColumn('users', 'confirm_change_email_token', $this->string()->unique());
	}
	
	public function down()
	{
		$this->dropColumn('users', 'confirm_change_email_token');
	}
}
