<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_socials}}`.
 */
class m210121_120925_create_users_socials_table extends Migration
{
	public function up()
	{
		$tableOptions = null;
		
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->createTable('socials', [
			'id' => $this->primaryKey(),
			'name' =>$this->string()->notNull(),
			'url' => $this->string()->notNull(),
			'logo' => $this->string(),
		], $tableOptions);
		
		$this->batchInsert('socials',['name', 'url'], [
			['name' => 'facebook', 'url' => 'https://www.facebook.com'],
			['name' => 'twitter', 'url' => 'https://twitter.com'],
			['name' => 'instagram', 'url' => 'https://www.instagram.com'],
			['name' => 'pinterest', 'url' => 'https://www.pinterest.com'],
			['name' => 'linkedin', 'url' => 'https://www.linkedin.com'],
			['name' => 'youtube', 'url' => 'https://www.youtube.com'],
			['name' => 'yelp', 'url' => 'https://www.yelp.com'],
			['name' => 'houzz', 'url' => 'https://www.houzz.ru'],
		]);
		
		$this->createTable('users_socials', [
			'user_id' => $this->integer()->notNull(),
			'social_id' =>$this->integer()->notNull(),
		], $tableOptions);
		
		
		$this->addForeignKey(
			'fk-users_socials-user_id',
			'users_socials',
			'user_id',
			'users',
			'id',
			'CASCADE',
			'CASCADE'
		);
		
		$this->addForeignKey(
			'fk-users_socials-social_id',
			'users_socials',
			'social_id',
			'socials',
			'id',
			'CASCADE',
			'CASCADE'
		);
		
		$this->createIndex(
			'idx-user-social',
			'users_socials',
			['user_id', 'social_id'],
			true
		);
	}
	
	public function down()
	{
		$this->dropForeignKey('fk-users_socials-user_id', 'users_socials');
		$this->dropForeignKey('fk-users_socials-social_id', 'users_socials');
		
		$this->dropTable('users_socials');
		
		$this->dropTable('socials');
	}
}
