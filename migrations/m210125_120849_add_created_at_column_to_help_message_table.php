<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%help_message}}`.
 */
class m210125_120849_add_created_at_column_to_help_message_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
	
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		
		$this->addColumn('help_messages', 'created_at', $this->dateTime()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropColumn('help_messages', 'created_at');
    }
}
