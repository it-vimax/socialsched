<?php

use yii\db\Migration;

/**
 * Class m210203_152802_added_token_field_and_old_email_for_change_email
 */
class m210203_152802_added_token_field_and_old_email_for_change_email extends Migration
{
	public function up()
	{
		$this->addColumn('users', 'email_confirm_token', $this->string()->unique()->after('email'));
		$this->addColumn('users', 'new_email', $this->string()->unique()->after('email_confirm_token'));
	}
	
	public function down()
	{
		$this->dropColumn('users', 'new_email');
		$this->dropColumn('users', 'email_confirm_token');
	}
}
