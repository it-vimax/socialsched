<?php

use yii\helpers\Html;

/* @var $user \app\models\UserModel */

$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['user/change-email-confirm', 'token' => $user->confirm_change_email_token]);
?>
<div class="password-reset">
	<p>Hello <?= Html::encode($user->username) ?>,</p>
	
	<p>Follow the link below to confirm your new email:</p>
	
	<p><?= Html::a(Html::encode($confirmLink), $confirmLink) ?></p>
</div>