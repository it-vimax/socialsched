<?php

namespace app\services;

use app\models\SignupForm;
use app\models\User;
use Yii;

class SignupService
{
	public function signup(SignupForm $form)
	{
		$user = new User();
		$user->status = User::STATUS_WAIT;
		$user->username = $form->username;
		$user->email = $form->email;
		$user->email_confirm_token = Yii::$app->security->generateRandomString();
		$user->first_name = $form->firstName;
		$user->last_name = $form->lastName;
		$user->birthdate = $form->birthdate;
		$user->gender = $form->gender;
		$user->setPassword($form->password);
		$user->generateAuthKey();
		$user->created_at = date('Y-m-d H:i:s');
		$user->updated_at = date('Y-m-d H:i:s');
		if(!$user->save()){
			throw new \RuntimeException('Saving error.');
		}
		
		return $user;
	}
	
	public function sentEmailConfirm(User $user)
	{
		$email = $user->email;
		
		$sent = Yii::$app->mailer
			->compose(
				['html' => 'user-signup-confirm-html', 'text' => 'user-signup-confirm-text'],
				['user' => $user])
			->setTo($email)
			->setFrom(Yii::$app->params['adminEmail'])
			->setSubject('Confirmation of registration')
			->send();
		
		if (!$sent) {
			throw new \RuntimeException('Sending error.');
		}
	}
	
	public function confirmation($token): void
	{
		if (empty($token)) {
			throw new \DomainException('Empty confirm token.');
		}
		
		$user = User::findOne(['email_confirm_token' => $token]);
		if (!$user) {
			throw new \DomainException('User is not found.');
		}
		
		$user->email_confirm_token = null;
		$user->status = User::STATUS_ACTIVE;
		if (!$user->save()) {
			throw new \RuntimeException('Saving error.');
		}
		
		if (!Yii::$app->getUser()->login($user)){
			throw new \RuntimeException('Error authentication.');
		}
	}
}
