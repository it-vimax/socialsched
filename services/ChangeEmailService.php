<?php

namespace app\services;

use app\models\User;
use app\models\UserModel;
use Yii;

class ChangeEmailService
{
	public function changeEmail(UserModel $userModel, $newEmail)
	{
		$userModel->new_email = $newEmail;
		$userModel->confirm_change_email_token = Yii::$app->security->generateRandomString();
		if(!$userModel->save()){
			throw new \RuntimeException('Saving error.');
		}
	}
	
	public function sendEmailConfirm(UserModel $user, $email)
	{
		$sent = Yii::$app->mailer
			->compose(
				['html' => 'user-change-email-confirm-html', 'text' => 'user-signup-confirm-text'],
				['user' => $user])
			->setTo($email)
			->setFrom(Yii::$app->params['adminEmail'])
			->setSubject('Confirmation of registration')
			->send();
		
		if (!$sent) {
			throw new \RuntimeException('Sending error.');
		}
	}
	
	public function confirmation($token): void
	{
		if (empty($token)) {
			throw new \DomainException('Empty confirm token.');
		}
		
		$user = UserModel::findOne(['confirm_change_email_token' => $token]);
		if (!$user) {
			throw new \DomainException('User is not found.');
		}
		
		$user->confirm_change_email_token = null;
		$user->email = $user->new_email;
		$user->new_email = null;
		if (!$user->save()) {
			throw new \RuntimeException('Saving error.');
		}
	}
}
