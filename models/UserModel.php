<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $birthdate
 * @property string $gender
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property string $new_email
 * @property string $confirm_change_email_token
 * @property int $status
 * @property string|null $photo
 * @property string|null $cover
 * @property int $created_at
 * @property int $updated_at
 * @property int $membership_id
 * @property int $publications_count
 */
class UserModel extends \yii\db\ActiveRecord
{
	const FILE_PHOTO = 'images/photos/';
	const FILE_COVER = 'images/covers/';
	
	const GENDER_NULL = '---';
	const GENDER_MALE = 'male';
	const GENDER_FEMALE = 'female';
	const GENDER_OTHER = 'other';
	
	public function getPhotoPath(): string
	{
	    return __DIR__ . '/../web/' . self::FILE_PHOTO . $this->photo;
	}
	
	public function getCoverPath(): string
	{
	    return __DIR__ . '/../web/' . self::FILE_COVER . $this->cover;
	}
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status','publications_count', 'membership_id', ], 'integer'],
            [['created_at', 'updated_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['username', 'password_hash', 'password_reset_token', 'confirm_change_email_token', 'email', 'new_email', 'photo', 'cover'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['new_email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['confirm_change_email_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'new_email' => 'Your new Email',
            'status' => 'Status',
            'photo' => 'Photo',
            'cover' => 'Cover',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	public function getSocials() {
		return $this->hasMany(Socials::className(), ['id' => 'social_id'])
			->viaTable('users_socials', ['user_id' => 'id']);
	}
	
	public function getHelpMessages() {
		return $this->hasMany(HelpMessages::className(), ['id' => 'user_id']);
	}
	
	public function isHasSocial($socialId)
	{
		return $this->hasMany(Socials::className(), ['id' => 'social_id'])
			->viaTable('users_socials', ['user_id' => 'id'])
			->andWhere(['socials.id' => $socialId])
			->scalar();
	}
	
	public static function statusList(): array
	{
		return [
			self::GENDER_NULL => '---',
			self::GENDER_MALE => 'MALE',
			self::GENDER_FEMALE => 'FEMALE',
			self::GENDER_OTHER => 'OTHER',
		];
	}
	
	public function getMembership()
	{
		return $this->hasOne(Memberships::className(), ['id' => 'membership_id']);
	}
	
	public function isCanPublish(): bool
	{
		return $this->publications_count > 0;
	}
}
