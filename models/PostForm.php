<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class PostForm extends Model
{
	public $status;
	public $title;
	public $body;
	/**
	 * @var UploadedFile $mediaFile attribute
	 */
	public $mediaFile;
	public $media;
	public $hashtag;
	public $website;
	public $date_publication_start;
	public $socials;
	
	public function uploadMediaFile()
	{
		$this->mediaFile = UploadedFile::getInstance($this, 'mediaFile');
		if ($this->validate()) {
			if (!empty($this->mediaFile)) {
				$this->media = time() . $this->mediaFile->baseName . '.' . $this->mediaFile->extension;
				if (!is_dir(Post::FILE_PATH)) {
					mkdir(Post::FILE_PATH, 0777, true);
				}
				$this->mediaFile->saveAs(Post::FILE_PATH . $this->media);
			}
			return true;
		} else {
			var_dump($this->errors);
			return false;
		}
	}
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['status', 'title', 'date_publication_start', 'body', 'socials'], 'required'],
			[['date_publication_start',], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
			[['title', 'media', 'website', 'hashtag'], 'string', 'max' => 250],
			[['status'], 'string', 'max' => 10],
			[['mediaFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
			['socials', 'each', 'rule' => ['integer']],
			['socials', 'socialsValidator'],
		];
	}
	
	public function socialsValidator($attributeName, $params)
	{
		$isError = false;
		$user = UserModel::findOne(['id' => Yii::$app->user->id]);
		
		if (empty(array_diff($this->socials, Socials::userSocialsList($user->id)))) {
			$this->addError('socials', 'You have entered an unknown social network.');
			$isError = true;
		}
		
		if (!empty($user->getMembership()->one())) {
			$platformsCount = (int) $user->getMembership()->one()->platforms_count;
			if ($platformsCount !== -1) {
				if (count($this->socials) > $platformsCount) {
					$this->addError('socials', 'The number of selected social networks has been exceeded.');
					$isError = true;
				};
			}
		} else {
			$this->addError('socials', 'You need selected membership.');
			$isError = true;
		}
		
		return $isError;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'socials' => 'Publish to',
			'mediaFile' => 'Add media',
		];
	}
}
