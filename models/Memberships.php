<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "memberships".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $level
 * @property int $platforms_count
 * @property int $publications_count
 * @property float $price
 */
class Memberships extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'memberships';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'level', 'platforms_count', 'publications_count', 'price'], 'required'],
            [['description'], 'string'],
            [['level', 'platforms_count', 'publications_count'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'level' => 'Level',
            'platforms_count' => 'Platforms Count',
            'publications_count' => 'Publications Count',
            'price' => 'Price',
        ];
    }
	
	public function getUsers() {
		return $this->hasMany(User::className(), ['id' => 'user_id']);
	}
	
	public function getUserModel() {
		return $this->hasMany(User::className(), ['id' => 'user_id']);
	}
}
