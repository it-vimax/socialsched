<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "socials".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string|null $logo
 */
class Socials extends \yii\db\ActiveRecord
{
	const FILE_PATH = 'images/socials/';
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'socials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            [['name', 'url', 'logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'logo' => 'Logo',
        ];
    }
	
    public static function findOther($userId)
    {
    	$subQuery = self::find()
			->select('socials.id')
			->joinWith('userModel', true)
			->where(['users_socials.user_id' => $userId]);
	
		return self::find()
			->where(['not in', 'id', $subQuery])
			->all();
    }
    
    public static function findByUser($userId)
    {
		return self::find()
			->joinWith('userModel', true)
			->where(['users_socials.user_id' => $userId])
			->all();
    }
    
    public static function findByUserWithoutPost($userId, $postId)
    {
    	$query = self::find()
			->select('socials.id')
			->joinWith('posts')
			->where(['posts_socials.post_id' => $postId]);
	
		return self::find()
			->joinWith('userModel', true)
			->andWhere(['users_socials.user_id' => $userId])
			->andWhere(['not in', 'socials.id', $query])
			->all();
    }
    
	public function getUserModel() {
		return $this->hasMany(UserModel::className(), ['id' => 'user_id'])
			->viaTable('users_socials', ['social_id' => 'id']);
	}
	
	public function getUser() {
		return $this->hasMany(User::className(), ['id' => 'user_id'])
			->viaTable('users_socials', ['social_id' => 'id']);
	}
	
	public function getPosts() {
		return $this->hasMany(Post::className(), ['id' => 'post_id'])
			->viaTable('posts_socials', ['social_id' => 'id']);
	}
	
	public static function userSocialsList($userId): array
	{
		$list = [];
		$userSocials = Socials::findByUser($userId);
		
		if (!empty($userSocials)) {
			/** @var Socials $social */
			foreach ($userSocials as $social) {
				$list[$social->id] = $social->name;
			};
		}
		
		return $list;
	}
}
