<?php

namespace app\models;

use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\web\UploadedFile;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property string $title
 * @property string $website
 * @property string $body
 * @property string|null $media
 * @property string|null $hashtag
 * @property string $date_publication_start
 * @property string|null $date_publication_end
 * @property string $created_at
 * @property string $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
	const FILE_PATH = 'images/posts/';
	
	const STATUS_ON = 'on';
	const STATUS_DRAFT = 'draft';
	
	public function isPublished(): bool
	{
		$publicationStartDate = new DateTime($this->date_publication_start);
		$now = new DateTime("now");
		
		return $publicationStartDate < $now;
	}
	
	public function getPath(): string
	{
		return !empty($this->media) ? '/' . self::FILE_PATH . $this->media : "";
	}
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'title', 'body', 'date_publication_start'], 'required'],
            [['date_publication_start', 'date_publication_end', 'created_at', 'updated_at'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['user_id'], 'integer'],
            [['body'], 'string'],
            [['status'], 'string', 'max' => 10],
            [['title', 'website', 'media', 'hashtag'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'title' => 'Title',
            'body' => 'Body',
            'media' => 'Media',
            'hashtag' => 'Hashtag',
            'date_publication_start' => 'Date Publication Start',
            'date_publication_end' => 'Date Publication End',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
	
	public function getSocials() {
		return $this->hasMany(Socials::className(), ['id' => 'social_id'])
			->viaTable('posts_socials', ['post_id' => 'id']);
	}
	
	public static function statusList(): array
	{
		return [
			self::STATUS_DRAFT => 'DRAFT',
			self::STATUS_ON => 'ON',
		];
	}
	
	public static function findByUserAndSocial($userId, $socialId)
	{
		return self::find()
			->joinWith('socials')
			->where(['posts.user_id' => $userId])
			->andWhere(['socials.id' => $socialId])
			->all();
	}
	
	public function getSocialsArray(): array
	{
		$socialList = [];
		/** @var Socials $social */
		foreach ($this->getSocials()->all() as $social) {
			$socialList[$social->id] = $social->name;
		}
		
		return $socialList;
	}
	
	public function socialsCheckedArr(array $allSocials): array
	{
		$postsList = [];
		/** @var Socials $social */
		foreach ($this->getSocials()->all() as $social) {
			$postsList[$social->id] = $social->name;
		}
		
		foreach ($allSocials as $userSocial) {
			foreach ($postsList as $postSocial) {
			    if ($userSocial[0] === $postSocial[0]) {
					$userSocial['checked'] = true;
			    }
			}
		}
		
		return $allSocials;
	}
}
