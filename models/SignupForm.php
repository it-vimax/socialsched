<?php

namespace app\models;

use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
	public $username;
	public $email;
	public $password;
	public $firstName;
	public $lastName;
	public $birthdate;
	public $birthdate_day;
	public $birthdate_month;
	public $birthdate_year;
	public $gender;
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['username', 'firstName', 'lastName',], 'trim'],
			[['username', 'firstName', 'lastName', 'birthdate', 'gender', 'birthdate_day', 'birthdate_month', 'birthdate_year',], 'required'],
			['username', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This username has already been taken.'],
			[['username', 'firstName', 'lastName',], 'string', 'min' => 2, 'max' => 255],
			['email', 'trim'],
			['email', 'required'],
			['email', 'email'],
			['email', 'string', 'max' => 255],
			['email', 'unique', 'targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.'],
			['password', 'required'],
			['password', 'string', 'min' => 6],
			[['birthdate',], 'date', 'format' => 'php:Y-m-d'],
			['birthdate', 'birthdateValidator'],
		];
	}
	
	public function birthdateValidator($attributeName, $params)
	{
		if (new \DateTimeImmutable($this->birthdate) >= new \DateTimeImmutable()) {
			$this->addError('birthdate', 'Your birthday cannot be more than the current date.');
			return false;
		}
		
		return true;
	}
	
	/**
	 * Signs user up.
	 *
	 * @return User|null the saved model or null if saving fails
	 */
	public function signup()
	{
		$this->birthdate = $this->birthdate_year . '-' . $this->birthdate_month . '-' . $this->birthdate_day;
		if (!$this->validate()) {
			return null;
		}
		
		$user = new User();
		$user->status = User::STATUS_DELETED;
		$user->username = $this->username;
		$user->email = $this->email;
		$user->first_name = $this->firstName;
		$user->last_name = $this->lastName;
		$user->birthdate = $this->birthdate;
		$user->gender = $this->gender;
		$user->setPassword($this->password);
		$user->generateAuthKey();
		$user->created_at = date('Y-m-d H:i:s');
		$user->updated_at = date('Y-m-d H:i:s');
		return $user->save() ? $user : null;
	}
	
	public static function getMonths()
	{
		return [
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		];
	}
	
	public function generateBirthDate(): void
	{
		$this->birthdate = $this->birthdate_year . '-' . $this->birthdate_month . '-' . $this->birthdate_day;
	}
	
	public static function getYears()
	{
		$res = [];
		for ($i=date('Y'); $i > 1960; $i--) {
			$res[$i] = $i;
		}
		return $res;
	}
	
	public static function getDays()
	{
		$res = [];
		for ($i=1; $i <= 31; $i++) {
			$res[$i] = $i;
		}
		return $res;
	}
}