<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UserUpdateForm extends Model
{
	public $username;
	public $first_name;
	public $last_name;
	public $birthdate;
	public $gender;
	public $oldPassword;
	public $newPassword;
	public $confirmPassword;
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['username', 'first_name', 'last_name', 'birthdate', 'gender',], 'required'],
			[['birthdate',], 'date', 'format' => 'php:Y-m-d'],
			[['gender'], 'string', 'max' => 10],
			[['username', 'first_name', 'last_name', 'birthdate', 'gender',], 'string', 'max' => 250],
			[['oldPassword','newPassword','confirmPassword',], 'string', 'min' => 6],
			['oldPassword','comparePassword'],
			['confirmPassword','isCanChange'],
			['confirmPassword', 'compare', 'compareAttribute'=>'newPassword', 'message'=>"Your current password is not correct." ],
		];
	}
	
	public function isCanChange($attributeName, $params)
	{
		$isError = false;
		if (empty($this->oldPassword)) {
			$isError = true;
			$this->addError('oldPassword', 'You need insert your old password.');
		}
		
		return $isError;
	}
	
	public function comparePassword($attributeName, $params)
	{
		$isError = false;
		$user = UserModel::findOne(['id' => Yii::$app->user->id]);
		if (!Yii::$app->getSecurity()->validatePassword($this->oldPassword, $user->password_hash)) {
			$isError = true;
			$this->addError('oldPassword', 'Your current password is not correct.');
		}

		return $isError;
	}
}