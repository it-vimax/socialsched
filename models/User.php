<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Class User
 * @package app\models
 * @property Post[] $posts
 */

class User extends ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
	const STATUS_ACTIVE = 10;
	const STATUS_WAIT = 5;
	
	public function behaviors()
	{
		return [
//			TimestampBehavior::className(),
		];
	}
	
	public function rules()
	{
		return [
			['status', 'default', 'value' => self::STATUS_DELETED],
			['status', 'in', 'range' => [self::STATUS_DELETED, self::STATUS_WAIT, self::STATUS_ACTIVE]],
		];
	}
	
	public static function findIdentity($id)
	{
		return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
	}
	
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}
	
	public static function findByUsername($username)
	{
		return static::findOne(['username' => $username]);
	}
	
	public static function findByPasswordResetToken($token)
	{
		
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		
		return static::findOne([
			'password_reset_token' => $token,
			'status' => self::STATUS_ACTIVE,
		]);
	}
	
	public static function isPasswordResetTokenValid($token)
	{
		
		if (empty($token)) {
			return false;
		}
		
		$timestamp = (int) substr($token, strrpos($token, '_') + 1);
		$expire = Yii::$app->params['user.passwordResetTokenExpire'];
		return $timestamp + $expire >= time();
	}
	
	public function getId()
	{
		return $this->getPrimaryKey();
	}
	
	public function getAuthKey()
	{
		return $this->auth_key;
	}
	
	public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}
	
	public function validatePassword($password)
	{
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}
	
	public function setPassword($password)
	{
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}
	
	public function generateAuthKey()
	{
		$this->auth_key = Yii::$app->security->generateRandomString();
	}
	
	public function generatePasswordResetToken()
	{
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}
	
	public function removePasswordResetToken()
	{
		$this->password_reset_token = null;
	}
	
	public static function findByEmail($email)
	{
		return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
	}
	
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPosts()
	{
		return $this->hasMany(Post::className(), ['user_id' => 'id']);
	}
	
	public function getSocials() {
		return $this->hasMany(Socials::className(), ['id' => 'social_id'])
			->viaTable('users_socials', ['user_id' => 'id']);
	}
	
	public function getMembership() {
		return $this->hasOne(Memberships::className(), ['id' => 'membership_id']);
	}
	
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'users';
	}
}