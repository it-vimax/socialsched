<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class UserHelpMessageForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }
	
	public function attributeLabels()
	{
		return [
			'name' => 'Your name',
			'email' => 'Your email',
			'subject' => 'Subject',
			'body' => 'Your message (optional)',
		];
	}
}
