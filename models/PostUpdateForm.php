<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class PostUpdateForm extends Model
{
	public $title;
	public $status;
	public $body;
	public $hashtag;
	public $website;
	public $date_publication_start;
	public $socials;
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['status', 'title', 'date_publication_start', 'body', 'socials'], 'required'],
			[['date_publication_start',], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
			[['status'], 'string', 'max' => 10],
			[['title', 'website', 'hashtag'], 'string', 'max' => 250],
			['socials', 'each', 'rule' => ['integer']],
			['socials', 'socialsValidator'],
		];
	}
	
	public function socialsValidator($attributeName, $params)
	{
		$isError = false;
		$user = UserModel::findOne(['id' => Yii::$app->user->id]);
		
		if (empty(array_diff($this->socials, Socials::userSocialsList($user->id)))) {
			$this->addError('socials', 'You have entered an unknown social network.');
			$isError = true;
		}
		
		if (count($this->socials) > $user->getMembership()->one()->platforms_count) {
			$this->addError('socials', 'The number of selected social networks has been exceeded.');
			$isError = true;
		};
		
		return !$isError;
	}
}
