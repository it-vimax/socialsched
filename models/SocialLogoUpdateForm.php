<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class SocialLogoUpdateForm extends Model
{
	/**
	 * @var UploadedFile $mediaFile attribute
	 */
	public $mediaFile;
	public $media;
	
	public function uploadMediaFile()
	{
		$this->mediaFile = UploadedFile::getInstance($this, 'mediaFile');
		if ($this->validate()) {
			if (!empty($this->mediaFile)) {
				$this->media = time() . $this->mediaFile->baseName . '.' . $this->mediaFile->extension;
				if (!is_dir(Socials::FILE_PATH)) {
					mkdir(Socials::FILE_PATH, 0777, true);
				}
				$this->mediaFile->saveAs(Socials::FILE_PATH . $this->media);
			}
			return true;
		} else {
			var_dump($this->errors);
			return false;
		}
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			['media', 'required'],
			['media', 'string', 'max' => 255],
			[['mediaFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'ico, svg, png, jpg'],
		];
	}
}
