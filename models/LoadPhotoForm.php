<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class LoadPhotoForm extends Model
{
	/**
	 * @var UploadedFile $photoFile
	 */
	public $photoFile;
	public $photo;
	
	public function uploadMediaFile()
	{
		$this->photoFile = UploadedFile::getInstance($this, 'photoFile');
		$this->photo = time() . $this->photoFile->baseName . '.' . $this->photoFile->extension;
		if ($this->validate()) {
			if (!is_dir(UserModel::FILE_PHOTO)) {
				mkdir(UserModel::FILE_PHOTO, 0777, true);
			}
			$this->photoFile->saveAs(UserModel::FILE_PHOTO . $this->photo);
			return true;
		} else {
			var_dump($this->errors);
			return false;
		}
	}
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['photo',], 'string', 'max' => 250],
			[['photoFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
		];
	}
}