<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class LoadCoverForm extends Model
{
	/**
	 * @var UploadedFile $coverFile attribute
	 */
	public $coverFile;
	public $cover;
	
	public function uploadMediaFile()
	{
		$this->coverFile = UploadedFile::getInstance($this, 'coverFile');
		$this->cover = time() . $this->coverFile->baseName . '.' . $this->coverFile->extension;
		if ($this->validate()) {
			if (!is_dir(UserModel::FILE_COVER)) {
				mkdir(UserModel::FILE_COVER, 0777, true);
			}
			$this->coverFile->saveAs(UserModel::FILE_COVER . $this->cover);
			return true;
		} else {
			var_dump($this->errors);
			return false;
		}
	}
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['cover',], 'string', 'max' => 250],
			[['coverFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
		];
	}
}