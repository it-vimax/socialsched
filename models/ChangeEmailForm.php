<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ChangeEmailForm extends Model
{
    public $email;
    
    public function rules()
    {
        return [
            [['email',], 'required'],
            ['email', 'email'],
            ['email', 'isUniqueNewEmail'],
        ];
    }
	
	public function isUniqueNewEmail($attribute, $params)
	{
		$isError = false;
		
		if(UserModel::findOne(['email' => $this->email])) {
			$isError = true;
			$this->addError('email', 'This email is already taken.');
		}
		
		$user = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
		if ($user->email === $this->email) {
			$isError = true;
			$this->addError('email', 'Your old email cannot be equal to your new email.');
		}
		
		return $isError;
	}
    
	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'email' => 'Your new Email',
		];
	}
}
