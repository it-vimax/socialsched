<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class PostUpdateMediaForm extends Model
{
	/**
	 * @var UploadedFile $mediaFile
	 */
	public $mediaFile;
	public $media;
	
	public function uploadMediaFile()
	{
		$this->mediaFile = UploadedFile::getInstance($this, 'mediaFile');
		$this->media = time() . $this->mediaFile->baseName . '.' . $this->mediaFile->extension;
		if ($this->validate()) {
			if (!is_dir(Post::FILE_PATH)) {
				mkdir(Post::FILE_PATH, 0777, true);
			}
			$this->mediaFile->saveAs(Post::FILE_PATH . $this->media);
			return true;
		} else {
			var_dump($this->errors);
			return false;
		}
	}
	
	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[['media',], 'string', 'max' => 250],
			[['mediaFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
		];
	}
}
