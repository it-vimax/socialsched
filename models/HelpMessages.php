<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "help_messages".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $username
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property string $created_at
 *
 * @property User $user
 */
class HelpMessages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'help_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id',], 'integer'],
            [['email', 'subject', 'body', 'created_at', 'username',], 'required'],
            [['body'], 'string'],
            [['email', 'subject'], 'string', 'max' => 255],
			[['created_at',], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'username' => 'User Name',
            'email' => 'Email',
            'subject' => 'Subject',
            'body' => 'Body',
            'created_at' => 'Create Date',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
