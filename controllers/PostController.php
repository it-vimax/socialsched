<?php

namespace app\controllers;

use app\models\PostForm;
use app\models\PostUpdateForm;
use app\models\PostUpdateMediaForm;
use app\models\Socials;
use app\models\UserModel;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\filters\AccessControl;
use yii\helpers\BaseHtml;
use yii\helpers\BaseStringHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'models' => $dataProvider->getModels(),
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	
    	if ($model->user_id !== Yii::$app->getUser()->getId()) {
			throw new NotFoundHttpException('Post is not found');
    	}
    	
        return $this->render('view', [
            'model' => $model,
        ]);
    }
	
	public function actionViewJson($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$model = $this->findModel($id);
		
		if ($model->user_id !== Yii::$app->getUser()->getId()) {
			throw new NotFoundHttpException('Post is not found');
		}
		
		$response = [];
		$response['id'] = $model->id;
		$response['title'] = $model->title;
		$response['body_short'] = BaseStringHelper::truncate($model->body, 50);
		$response['body'] = $model->body;
		$response['media'] = '/' . Post::FILE_PATH . $model->media;
		$response['hashtag'] = $model->hashtag;
		$response['website'] = $model->website;
		$response['date_publication_start'] = $model->date_publication_start;
		$response['is_published'] = $model->isPublished();
		
		return $this->asJson($response);
	}

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $modelForm = new PostForm();

        $posts = Yii::$app->request->post();
        if ($modelForm->load($posts) && $modelForm->validate()) {
			$user = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
			if (!$user->isCanPublish()) {
				Yii::$app->session->setFlash('warning', 'You have run out of attempts to create growth.');
				return $this->redirect(['pricing-plans/index']);
			}
			
			if($modelForm->uploadMediaFile()) {
				$model = new Post();
				$model->user_id = Yii::$app->getUser()->getId();
				$model->created_at = date('Y-m-d H:m:s');
				$model->updated_at = date('Y-m-d H:m:s');
				$model->media = $modelForm->media;
				$model->status = $modelForm->status;
				$model->title = $modelForm->title;
				$model->body = $modelForm->body;
				$model->hashtag = $modelForm->hashtag;
				$model->date_publication_start = $modelForm->date_publication_start;
				if ($model->save()) {
					$user->publications_count = $user->publications_count - 1;
					$user->save();
					
					foreach ($modelForm->socials as $socialId) {
						$model->link('socials', Socials::findOne($socialId));
					}
					
					Yii::$app->session->setFlash('success', 'New post created successful.');
					return $this->redirect(['view', 'id' => $model->id]);
				} else {
					Yii::$app->session->setFlash('error', 'Error save new Post');
				}
			} else {
				Yii::$app->session->setFlash('error', 'Error upload media file for new post');
			}
        }
        
        return $this->render('create', [
            'model' => $modelForm,
        ]);
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
		if ($model->user_id !== Yii::$app->getUser()->getId()) {
			throw new NotFoundHttpException('Post is not found');
		}
	
		$modelForm = new PostUpdateForm();
		$modelForm->title = $model->title;
		$modelForm->status = $model->status;
		$modelForm->website = $model->website;
		$modelForm->body = $model->body;
		$modelForm->hashtag = $model->hashtag;
		$modelForm->date_publication_start = $model->date_publication_start;

		if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
			$model->title = $modelForm->title;
			$model->body = $modelForm->body;
			$model->website = $modelForm->website;
			$model->status = $modelForm->status;
			$model->hashtag = $modelForm->hashtag;
			$model->date_publication_start = $modelForm->date_publication_start;
			$model->updated_at = date('Y-m-d H:m:s');
			if ($model->save()) {
				$model->unlinkAll('socials', true);
				foreach ($modelForm->socials as $socialId) {
					$model->link('socials', Socials::findOne($socialId));
				}
				
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}
	
		$postUpdateMediaForm = new PostUpdateMediaForm();
	
		return $this->render('update', [
			'model' => $model,
			'modelForm' => $modelForm,
			'postUpdateMediaForm' => $postUpdateMediaForm,
		]);
	}
    
    public function actionUpdateMediaFile($id)
    {
		$model = $this->findModel($id);
	
		if ($model->user_id !== Yii::$app->getUser()->getId()) {
			throw new NotFoundHttpException('Post is not found');
		}
	
		$modelForm = new PostUpdateMediaForm();
		$posts = Yii::$app->request->post();
		if ($modelForm->load($posts)) {
			if (!empty($model->media)) {
				$pathToFile = __DIR__ . '/../web/' . Post::FILE_PATH . $model->media;
				if (file_exists($pathToFile)) {
					unlink($pathToFile);
				}
				$model->media = null;
				$model->save();
			}
			if($modelForm->uploadMediaFile()) {
				$model->media = $modelForm->media;
				$model->updated_at = date('Y-m-d H:m:s');
				$model->save();
			}
		}
		
		return $this->redirect(['/post/update', 'id' => $model->id]);
    }

    public function actionDeleteMediaFile($id)
    {
		$model = $this->findModel($id);
	
		if ($model->user_id !== Yii::$app->getUser()->getId()) {
			throw new NotFoundHttpException('Post is not found');
		}
	
		if(!empty($model->media)) {
			$pathToFile = __DIR__ . '/../web/' . Post::FILE_PATH . $model->media;
			if (file_exists($pathToFile)) {
				unlink($pathToFile);
			}
			$model->media = null;
			$model->updated_at = date('Y-m-d H:m:s');
			if (!$model->save()) {
				var_dump($model->errors);
			}
		}
		
		return $this->redirect(['/post/update', 'id' => $model->id]);
    }
    
    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
	
		if ($model->user_id !== Yii::$app->getUser()->getId()) {
			throw new NotFoundHttpException('Post is not found');
		}
	
		if(!empty($model->media)) {
			$pathToFile = __DIR__ . '/../web/' . Post::FILE_PATH . $model->media;
			if (file_exists($pathToFile)) {
				unlink($pathToFile);
			}
		}
		
		$model->delete();
		
        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
