<?php

namespace app\controllers;

use app\models\HelpMessages;
use app\models\Post;
use app\models\Socials;
use app\models\UserHelpMessageForm;
use app\models\UserModel;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class UserHelpMessageController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
	
	/**
	 * @return string
	 */
	public function actionCreate()
	{
		$userModel = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
		$model = new UserHelpMessageForm();
		
		$model->name = $userModel->username;
		$model->email = $userModel->email;
		
		$posts = Yii::$app->request->post();
		if ($model->load($posts) && $model->validate()) {
			$helpMessageModel = new HelpMessages();
			$helpMessageModel->user_id = Yii::$app->getUser()->getId();
			$helpMessageModel->username = $model->name;
			$helpMessageModel->email = $model->email;
			$helpMessageModel->subject = $model->subject;
			$helpMessageModel->body = $model->body;
			$helpMessageModel->created_at = date('Y-m-d H:i:s');
			if ($helpMessageModel->save()) {
				//TODO send email ??
				
				Yii::$app->session->setFlash('success', 'Your message send successfully.');
				return $this->redirect(['site/index']);
			}else{
				Yii::$app->session->setFlash('error', 'Error sending your message.');
			}
		}
		
		return $this->render('create', [
			'userModel' => $userModel,
			'model' => $model,
		]);
	}
}