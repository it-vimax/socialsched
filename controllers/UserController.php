<?php

namespace app\controllers;

use app\models\ChangeEmailForm;
use app\models\LoadCoverForm;
use app\models\LoadPhotoForm;
use app\models\UserUpdateForm;
use app\services\ChangeEmailService;
use app\services\SignupService;
use Yii;
use app\models\UserModel;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for UserModel model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
	
	/**
	 * @return string
	 * @throws NotFoundHttpException
	 */
    public function actionView()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->getUser()->getId()),
        ]);
    }

    /**
     * Updates an existing UserModel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->getUser()->getId());
  
		$modelForm = new UserUpdateForm();
		$modelForm->username = $model->username;
		$modelForm->first_name = $model->first_name;
		$modelForm->last_name = $model->last_name;
		$modelForm->birthdate = $model->birthdate;
		$modelForm->gender = $model->gender;
	
		if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
			$model->username = $modelForm->username;
			$model->first_name = $modelForm->first_name;
			$model->birthdate = $modelForm->birthdate;
			$model->last_name = $modelForm->last_name;
			$model->gender = $modelForm->gender;
			$model->updated_at = date('Y-m-d H:m:s');
			if (!empty($modelForm->newPassword)) {
				$model->password_hash = Yii::$app->security->generatePasswordHash($modelForm->newPassword);
			}
			if ($model->save()) {
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
				var_dump($model->errors);
			}
		}
	
		$changeEmailForm = new ChangeEmailForm();
		$changeEmailForm->email = $model->new_email;
		
		return $this->render('update', [
			'model' => $model,
			'modelForm' => $modelForm,
			'loadPhotoModelForm' => new LoadPhotoForm(),
			'loadCoverModelForm' => new LoadCoverForm(),
			'changeEmailForm' => $changeEmailForm,
		]);
	}
	
	public function actionUpdatePhoto()
	{
		$model = $this->findModel(Yii::$app->getUser()->getId());
		
		$modelForm = new LoadPhotoForm();
		$photoPost = Yii::$app->request->post();
		if ($modelForm->load($photoPost)) {
			if (!empty($model->photo)) {
				$pathToFile = $model->getPhotoPath();
				if (file_exists($pathToFile)) {
					unlink($pathToFile);
				}
				$model->photo = null;
				$model->save();
			}
			if($modelForm->uploadMediaFile()) {
				$model->photo = $modelForm->photo;
				$model->updated_at = date('Y-m-d H:m:s');
				$model->save();
			}
		}
		
		return $this->redirect(['/user/update']);
	}
	
	public function actionDeletePhoto()
	{
		$model = $this->findModel(Yii::$app->getUser()->getId());
		
		if(!empty($model->photo)) {
			$pathToFile = $model->getPhotoPath();
			if (file_exists($pathToFile)) {
				unlink($pathToFile);
			}
			$model->photo = null;
			$model->updated_at = date('Y-m-d H:m:s');
			if (!$model->save()) {
				var_dump($model->errors);
			}
		}
		
		return $this->redirect(['/user/update']);
	}
	
	public function actionUpdateCover()
	{
		$model = $this->findModel(Yii::$app->getUser()->getId());
		
		$modelForm = new LoadCoverForm();
		$coverPost = Yii::$app->request->post();
		if ($modelForm->load($coverPost)) {
			if (!empty($model->cover)) {
				$pathToFile = $model->getCoverPath();
				if (file_exists($pathToFile)) {
					unlink($pathToFile);
				}
				$model->cover = null;
				$model->save();
			}
			if($modelForm->uploadMediaFile()) {
				$model->cover = $modelForm->cover;
				$model->updated_at = date('Y-m-d H:m:s');
				$model->save();
			}
		}
		
		return $this->redirect(['/user/update']);
	}
	
	public function actionDeleteCover()
	{
		$model = $this->findModel(Yii::$app->getUser()->getId());
		
		if(!empty($model->cover)) {
			$pathToFile = $model->getCoverPath();
			if (file_exists($pathToFile)) {
				unlink($pathToFile);
			}
			$model->cover = null;
			$model->updated_at = date('Y-m-d H:m:s');
			if (!$model->save()) {
				var_dump($model->errors);
			}
		}
		
		return $this->redirect(['/user/update']);
	}
    
	public function actionChangeEmail()
	{
		$model = $this->findModel(Yii::$app->getUser()->getId());
		$modelForm = new ChangeEmailForm();
		$changeEmailService = new ChangeEmailService();
		if ($modelForm->load(Yii::$app->request->post()) && $modelForm->validate()) {
			$changeEmailService->changeEmail($model, $modelForm->email);
			$changeEmailService->sendEmailConfirm($model, $modelForm->email);
		}
		
		return $this->redirect(['/user/update']);
	}
	
	public function actionChangeEmailConfirm($token)
	{
		try{
			$changeEmailService = new ChangeEmailService();
			$changeEmailService->confirmation($token);
			Yii::$app->session->setFlash('success', 'You have successfully confirmed your email.');
		} catch (\Exception $e){
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', $e->getMessage());
		}
		
		return $this->redirect(['/user/update']);
	}
	
    /**
     * Finds the UserModel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserModel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
