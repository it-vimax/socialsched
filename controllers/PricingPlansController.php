<?php

namespace app\controllers;

use app\models\Memberships;
use app\models\UserModel;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SocialController implements the CRUD actions for Socials model.
 */
class PricingPlansController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'Buy' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Socials models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'models' => Memberships::find()->all(),
        ]);
    }
	
	/**
	 * @param $id
	 */
    public function actionBuy($id)
    {
		$memberships = $this->findModel($id);
        $user = UserModel::findOne(Yii::$app->getUser()->getId());
		$user->link('membership', $memberships);
		$user->publications_count = $memberships->publications_count;
		if (!$user->save()) {
			throw new \DomainException('Error add publication items');
		}
		
		Yii::$app->session->setFlash('success', 'You bought an ' . $memberships->name . ' publication item.');
		
		return $this->redirect('/post/create');
    }
	
	/**
	 * @param $id
	 * @return Memberships|null
	 * @throws NotFoundHttpException
	 */
    protected function findModel($id)
    {
        if (($model = Memberships::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
