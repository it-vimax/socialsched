<?php

namespace app\controllers;

use app\models\SocialForm;
use Yii;
use app\models\Socials;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SocialController implements the CRUD actions for Socials model.
 */
class SocialController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Socials models.
     * @return mixed
     */
    public function actionIndex()
    {
    	if (Yii::$app->getUser()->getId() !== 1) {
			throw new NotFoundHttpException('Page not found.');
    	}
    	
        $dataProvider = new ActiveDataProvider([
            'query' => Socials::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Socials model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
		if (Yii::$app->getUser()->getId() !== 1) {
			throw new NotFoundHttpException('Page not found.');
		}
		
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Socials model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (Yii::$app->getUser()->getId() !== 1) {
			throw new NotFoundHttpException('Page not found.');
		}
	
		$modelForm = new SocialForm();
		$posts = Yii::$app->request->post();
		if ($modelForm->load($posts) && $modelForm->validate()) {
			if($modelForm->uploadMediaFile()) {
				$model = new Socials();
				$model->name = $modelForm->name;
				$model->url = $modelForm->url;
				$model->logo = $modelForm->media;
				if ($model->save()) {
					return $this->redirect(['view', 'id' => $model->id]);
				}
			}
		}

        return $this->render('create', [
            'model' => $modelForm,
        ]);
    }

    /**
     * Updates an existing Socials model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
		if (Yii::$app->getUser()->getId() !== 1) {
			throw new NotFoundHttpException('Page not found.');
		}
		
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Socials model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
		if (Yii::$app->getUser()->getId() !== 1) {
			throw new NotFoundHttpException('Page not found.');
		}
		
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Socials model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Socials the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Socials::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
