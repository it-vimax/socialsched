<?php

namespace app\controllers;

use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\User;
use app\services\SignupService;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;

class SiteController extends Controller
{
	
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['login', 'logout', 'signup', 'calendar'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['login', 'signup'],
						'roles' => ['?'],
					],
					[
						'allow' => true,
						'actions' => ['logout', 'calendar'],
						'roles' => ['@'],
					],
				],
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
    	if (!Yii::$app->getUser()->isGuest) {
			return $this->redirect('site/calendar');
    	}
		$this->layout = 'base';
        return $this->render('index');
    }
    
    public function actionCalendar()
    {
    	$user = User::findOne(['id' => Yii::$app->getUser()->getId()]);
    	
		return $this->render('calendar', ['model' => $user]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
			try{
				if($model->login()){
					return $this->goBack();
				}
			} catch (\DomainException $e) {
				Yii::$app->session->setFlash('error', $e->getMessage());
				return $this->goBack();
			}
        }
		$this->layout = 'base';
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	
	public function actionSignup()
	{
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			try{
				$model->birthdate_day = Yii::$app->request->post('birthdate_day');
				$model->birthdate_month = Yii::$app->request->post('birthdate_month');
				$model->birthdate_year = Yii::$app->request->post('birthdate_year');
				$model->generateBirthDate();
				if ($model->validate()) {
					$signupService = new SignupService();
					$user = $signupService->signup($model);
					Yii::$app->session->setFlash('success', 'Check your email to confirm the registration.');
					$signupService->sentEmailConfirm($user);
					return $this->goHome();
				}
				
			} catch (\RuntimeException $e){
				Yii::$app->errorHandler->logException($e);
				Yii::$app->session->setFlash('error', $e->getMessage());
			}
		}
		
		$this->layout = 'base';
		return $this->render('signup', [
			'model' => $model,
		]);
	}
	
	public function actionSignupConfirm($token)
	{
		try{
			$signupService = new SignupService();
			$signupService->confirmation($token);
			Yii::$app->session->setFlash('success', 'You have successfully confirmed your registration.');
		} catch (\Exception $e){
			Yii::$app->errorHandler->logException($e);
			Yii::$app->session->setFlash('error', $e->getMessage());
		}
		
		return $this->goHome();
	}
	
	/**
	 * Requests password reset.
	 * @return mixed
	 */
	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();
		
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
				return $this->goHome();
			} else {
				Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
			}
		}
		$this->layout = 'base';
		return $this->render('requestPasswordReset', [
			'model' => $model,
		]);
	}
	
	/**
	 * Resets password.
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}
		
		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->session->setFlash('success', 'New password was saved.');
			return $this->goHome();
		}
		
		return $this->render('resetPassword', [
			'model' => $model,
		]);
      }
}
