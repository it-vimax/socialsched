<?php

namespace app\controllers;

use app\models\Post;
use app\models\Socials;
use app\models\UserModel;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PostSocialController extends Controller
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'DetachSocial' => ['POST'],
					'AttachSocial' => ['POST'],
				],
			],
		];
	}
	
	public function actionAttachSocial($postId, $socialId)
	{
		$user = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
		if (!$user->isHasSocial($socialId)) {
			throw new NotFoundHttpException('Social is detached from users.');
		}
		
		$post = $this->findPostModel($postId);
		if ($post->user_id !== $user->id) {
			throw new NotFoundHttpException('Post is not found');
		}
		
		$social = Socials::findOne($socialId);
		
		$post->link('socials', $social);
		
		return $this->redirect(['post/update', 'id' => $post->id]);
	}
	
	public function actionDetachSocial($postId, $socialId)
	{
		$user = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
		if (!$user->isHasSocial($socialId)) {
			throw new NotFoundHttpException('Social is detached from users.');
		}
		
		$post = $this->findPostModel($postId);
		if ($post->user_id !== $user->id) {
			throw new NotFoundHttpException('Post is not found');
		}
		
		$social = Socials::findOne($socialId);
		
		$post->unlink('socials', $social, true);
		
		return $this->redirect(['post/update', 'id' => $post->id]);
	}
	
	protected function findPostModel($id)
	{
		if (($model = Post::findOne($id)) !== null) {
			return $model;
		}
		
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}

