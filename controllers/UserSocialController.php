<?php

namespace app\controllers;

use app\models\Post;
use app\models\UserModel;
use Yii;
use app\models\Socials;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SocialController implements the CRUD actions for Socials model.
 */
class UserSocialController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'DetachSocial' => ['POST'],
                    'AttachSocial' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Socials models.
     * @return mixed
     */
    public function actionIndex()
    {
		$otherSocials = Socials::findOther(Yii::$app->getUser()->getId());
		$userSocials = Socials::findByUser(Yii::$app->getUser()->getId());
		
        return $this->render('index', [
        	'otherSocials' => $otherSocials,
        	'userSocials' => $userSocials,
		]);
    }
    
    public function actionAttachSocial($id)
    {
		if (!$social = Socials::findOne($id)) {
			throw new NotFoundHttpException('Social is not exist.');
		}
		
		$user = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
		
        if ($user->isHasSocial($id)) {
			throw new NotFoundHttpException('Social is attached.');
        }
	
		$user->link('socials', $social);
        
        Yii::$app->session->setFlash('success', "Attached social " . $social->name . ' is successful.');
        
        return $this->redirect('/user-social/index');
    }
    
    public function actionDetachSocial($id)
    {
		if (!$social = Socials::findOne($id)) {
			throw new NotFoundHttpException('Social is not exist.');
		}
		
		$user = UserModel::findOne(['id' => Yii::$app->getUser()->getId()]);
		
        if (!$user->isHasSocial($id)) {
			throw new NotFoundHttpException('Social is detached.');
        }
        
		$posts = Post::findByUserAndSocial($user->id, $id);
        if (!empty($posts)) {
        	/** @var Post $post */
        	foreach ($posts as $post) {
				$post->unlink('socials', $social, true);
        	}
        }
  
		$user->unlink('socials', $social, true);
	
		Yii::$app->session->setFlash('success', "Detach social " . $social->name . ' is successful.');
        
        return $this->redirect('/user-social/index');
    }
}
