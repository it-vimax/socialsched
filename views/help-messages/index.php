<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HelpMessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Help Messages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="help-messages-index page">

    <div class="page__wrapper py-5">
        <div class="d-flex align-items-center justify-content-between mb-5">
            <h1 class="page__title"><?= Html::encode($this->title) ?></h1>
            <div><?= Html::a('Create Help Messages', ['create'], ['class' => 'btn btn-lg btn-primary']) ?></div>
        </div>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <div class="socials__table">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    [                      
                        // the owner name of the model
                        'label' => 'Owner',
                        'value' => 'user.username',
                    ],
                    'username',
                    'email:email',
                    'subject',
                    'body:ntext',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>


    </div>
</div>
