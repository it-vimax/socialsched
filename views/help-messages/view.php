<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HelpMessages */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Help Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="help-messages-view page">
    <div class="page__wrapper py-5">
        <div class="d-flex align-items-center justify-content-between mb-5">
            <h1 class="page__title"><?= Html::encode($this->title) ?></h1>
            <div>
                <?php // Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-lg btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-lg btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>    
            </div>
        </div>
    
        <div class="socials__table">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
        			[                      // the owner name of the model
        				'label' => 'Owner',
        				'value' => $model->getUser()->one()->username,
        			],
                    'email:email',
                    'subject',
                    'body:ntext',
                    'created_at',
                ],
            ]) ?>
        </div>
    </div>

</div>
