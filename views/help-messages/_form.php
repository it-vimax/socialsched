<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\HelpMessages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="help-messages-form bg-white">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
	
    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>
	
    <?= $form->field($model, 'created_at', ['inputOptions' => [
        'autocomplete' => 'off']])->widget(DateTimePicker::class, [
        'name' => 'birthdate',
        'options' => ['placeholder' => 'Select date publication ...'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-MM-dd HH:mm:ss',
            'startDate' => date('Y-m-d H:i:s'),
            'todayHighlight' => true
        ]
    ]) ?>	
	
    <div class="btn-group">
        <?= Html::submitButton('Save Message', ['class' => 'btn btn-lg btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
