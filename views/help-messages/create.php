<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HelpMessages */

$this->title = 'Create Help Messages';
$this->params['breadcrumbs'][] = ['label' => 'Help Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="help-messages-create page">
    <div class="page__wrapper py-5">
        <h1 class="page__title mb-5"><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
