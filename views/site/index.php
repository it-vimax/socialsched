<?php

use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */

$this->title = 'Home';

?>


<div class="hero" style="background-image: url('/images/hero.jpg');">
	<div class="hero__content">
		<h1 class="hero__title">Intranet & Extranet <br>Become <span>Simple</span></h1>
		<p class="hero__description">Take full advantage of an intranet or extranet platform for your <br>company. In a few steps.</p>
		<a class="btn btn-lg btn-primary" href="<?php echo \yii\helpers\Url::toRoute(['/site/login']); ?>">Login</a>
	</div>
</div>


<div class="events">
	<div class="container">
		<div class="row d-flex flex-row align-items-center">
			<div class="col-md-7">
				<img src="/images/section-calendar-1024x683.png" alt="">
			</div>
			<div class="col-md-5 events__content">
				<h2 class="events__title">Internal events</h2>
				<p class="events__description">Discover our events that will allow you to strengthen the links between the different members of our company</p>
				<a class="btn btn-primary btn-lg" href="#">View all events</a>
			</div>
		</div>
	</div>
</div>

<div class="forums">
	<div class="container">
		<div class="row d-flex flex-row align-items-center">
			<div class="col-md-5 forums__content">
				<h2 class="forums__title">Community Forums</h2>
				<p class="forums__description">oin our private forum to share your ideas, desires and work with all members.</p>
				<a class="btn btn-secondary btn-lg" href="#">Visit forums</a>
			</div>
			<div class="col-md-7">
				<img src="/images/section-forum-1.png" alt="">
			</div>
		</div>
	</div>
</div>

<div class="platform">
	<div class="platform__content container-fluid">
		<div class="row">
			<div class="col-sm-12 text-center">
				<h2 class="platform__title">Why Use our Intranet Platform?</h2>
				<p class="platform__subtitle">3 SIMPLE REASONS</p>
			</div>
			<div class="platform__blocks col-sm-12">
				<div class="row ">
					<div class="platform__item col-md-4 ">
						<div class="platform__item_content">
							<div class="platform__icon platform__icon--red"><i class="fa fa-user-o"></i></div>
							<div class="platform__item_title">Your team gathered</div>
							<div class="platform__item_description">In consectetuer turpis ut velit. Etiam sit amet orci eget eros faucibus tincidunt.</div>
						</div>
					</div>
					<div class="platform__item col-md-4">
						<div class="platform__item_content">
							<div class="platform__icon platform__icon--blue"><i class="fa fa-hdd"></i></div>
							<div class="platform__item_title">Share documents</div>
							<div class="platform__item_description">In consectetuer turpis ut velit. Etiam sit amet orci eget eros faucibus tincidunt.</div>
						</div>
					</div>
					<div class="platform__item col-md-4">
						<div class="platform__item_content">
							<div class="platform__icon  platform__icon--green"><i class="fa fa-comment-o"></i></div>
							<div class="platform__item_title">Discuss your projects</div>
							<div class="platform__item_description">In consectetuer turpis ut velit. Etiam sit amet orci eget eros faucibus tincidunt.</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="help" style="background-image: url('/images/timj-6BVinN0Y7Xk-unsplash-1850x1389.jpg');">
	<div class="help__content">
		<h1 class="help__title">We are all close together</h1>
		<p class="help__description">A problem, a question, an emergency? <br>Do not hesitate to visit the help centre, <i>we can help you</i>.</p>
		<a class="btn btn-lg btn-secondary" href="#">Help center</a>
	</div>
</div>

<footer class="">
	<div class="row d-flex flex-row justify-content-between align-items-center">
		<p class="m-0 col-md-6">Copyright © 2019 Cera. All rights reserved</p>
		<ul class="col-md-6">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-slack"></i></a></li>
		</ul>
	</div>
</footer>


