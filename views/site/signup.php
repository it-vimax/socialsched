<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\SignupForm
 */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login_page container-fluid px-0">
	<div class="login_page__content col-lg-5 col-sm-12 px-0">
		<div class="login_page__content_header">
			<h1 class="page_title">Register</h1>
		</div>
		<div class="login_page__content_form">
			<?php $form = ActiveForm::begin([
				'options' => [
					'class' => 'standard-form clearfix',
				],
				'id' => 'login-form',
				'fieldConfig' => [
					'template' => "<div class=\"form-label-group mb-4\">{label}{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
					'labelOptions' => ['class' => ''],
				],
			]); ?>
				<div class="container container--narrow">
					<p class="d-none">Registering for this site is easy. Just fill in the fields below, and we'll get a new account set up for you in no time.</p>
					<div class="register-sections">
						<div class="register-section" id="basic-details-section">
							<h4 class="entry-title">Account Details </h4>
							
							<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
							<?= $form->field($model, 'email') ?>
							<?= $form->field($model, 'password')->passwordInput() ?>
						</div><!-- #basic-details-section -->

						<div class="register-section" id="profile-details-section">
							<h4 class="entry-title">Profile Details </h4>
							<?= $form->field($model, 'firstName')->textInput() ?>
							<?= $form->field($model, 'lastName')->textInput() ?>

							<div class="form-label-group mb-4">
								<label> Birthdate <span class="bp-required-field-label">(required)</span> </label>
								<div class="input-options datebox-selects row">
									<div class="col-lg-4">
										<label for="field_4_day" class="xprofile-field-label">Day</label>
										<?php echo Html::dropDownList('birthdate_day', 'null', \app\models\SignupForm::getDays(), [
											'class' => 'form-control',
										]); ?>
									</div>
									<div class="col-lg-4">
										<label for="field_4_day" class="xprofile-field-label">Month</label>
										<?php echo Html::dropDownList('birthdate_month', 'null', \app\models\SignupForm::getMonths(), [
											'class' => 'form-control',
										]); ?>
									</div>
									<div class="col-lg-4">
										<label for="field_4_day" class="xprofile-field-label">Year</label>
										<?php echo Html::dropDownList('birthdate_year', 'null', \app\models\SignupForm::getYears(), [
											'class' => 'form-control',
										]); ?>
									</div>
								</div>
							</div>
							
							<?= $form->field($model, 'gender')->dropDownList(\app\models\UserModel::statusList()) ?>
						</div><!-- #profile-details-section -->
					</div>

					<div class="submit d-flex flex-row justify-content-between align-items-center">
						<?= Html::submitButton('Complete Sign Up', ['class' => 'btn btn-primary btn-lg', 'name' => 'signup-button']) ?>
						<a href="<?php echo \yii\helpers\Url::toRoute(['site/login']); ?>">Login</a>
					</div>
				</div> <!-- .container -->
			<?php ActiveForm::end(); ?>
		</div>
	</div>
	<div class="login_page__background col-lg-7 col-sm-12" style="background-image: url('/images/page-login.jpg');"></div>
</div>