<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Log In';
/**
 * @var $model \app\models\PasswordResetRequestForm
 */
?>




<div class="login_page container-fluid px-0">
	<div class="login_page__content col-lg-5 col-sm-12 px-0">
		<div class="login_page__content_header">
			<h1 class="page_title"><?php echo $this->title; ?></h1>
		</div>
		<div class="login_page__content_form">

			<p class="message">Please enter your email address. You will receive an email message with instructions on how to reset your password.</p>
			
			<?php $form = ActiveForm::begin([
				'options' => [
					'class' => 'form-signin',
				],
				'id' => 'login-form',
				'fieldConfig' => [
					'template' => "<div class=\"form-label-group mb-4\">{label}{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
					'labelOptions' => ['class' => ''],
				],
			]); ?>
			<?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'form-control', 'required'=>true,]) ?>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
			<ul class="login_page__actions">
				<li><a href="<?php echo \yii\helpers\Url::toRoute(['/site/login']); ?>">Log In</a></li>
				<li><a href="<?php echo \yii\helpers\Url::toRoute(['/site/signup']); ?>">Register</a></li>
			</ul>
			<?php ActiveForm::end(); ?>

		</div>
	</div>
	<div class="login_page__background col-lg-7 col-sm-12" style="background-image: url('/images/page-login.jpg');"></div>
</div>





