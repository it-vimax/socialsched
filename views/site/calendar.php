<?php

use app\models\Post;
use edofre\fullcalendar\models\Event;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this yii\web\View
 * @var $model \app\models\User
 */

$this->title = 'Calendar';

$events = [];
if ($model->getPosts()->where(['status' => Post::STATUS_ON])->count() > 0) {
	/** @var \app\models\Post $post */
	foreach ($model->getPosts()->where(['status' => Post::STATUS_ON])->all() as $post) {
		$events[] = new Event([
			'id' => $post->id,
			'title' => $post->title,
			'start' => $post->date_publication_start,
			'url' => Html::encode(Url::toRoute(['/post/view', 'id' => $post->id])),
			'className' => ['click', 'event-id-' . $post->id ],
		]);
	}
}
?>

<div class="page">
	<header class="page__header" style="background-image: url('/images/header-default-events.jpg');">
		<div class="page__wrapper">
			<div class="page__header_content">
				<h4 class="page__subtitle">Events</h4>
				<h1 class="page__title">Schedule Your Posts!</h1>
				<p class="page__description">Take part in our special community events to meet new people or just have fun!</p>
			</div>
		</div>
	</header>
	
	<section class="page__section">
		<div class="page__wrapper">
			<div class="calendar">
				<?php echo edofre\fullcalendar\Fullcalendar::widget([
					'events' => $events
				]);?>
			</div>
		</div>
	</section>
</div>

<script>


	window.addEventListener("DOMContentLoaded", function() {

		let url = window.location.origin;

		let eventContainer = document.createElement('div')
		eventContainer.classList.add('calendar_post')
		document.querySelector('body').append(eventContainer);

		setTimeout(() => {
			$('.fc-event').hover(function(event){
				event.preventDefault();
				event.stopPropagation();
				let targetClaass = $(this).attr('href').split('id=');
				let target = event.target;
				let offset = $(target).offset();
				$.ajax({
					url: `${url}/post/view-json?id=${targetClaass[1]}`,
					success: function(responce){
						console.log(responce);
						let popup = `
							${responce.media ? '<div class="calendar_post__thumb"><img src=" '+ responce.media +' " alt=""></div>' : ''}
							<div class="calendar_post__wrapper">
								<h2 class="calendar_post__title">${responce.title}</h2>
								${responce.is_published ? '<div class="badge badge-pill badge-success">Publicated</div>' : '<div class="badge badge-pill badge-secondary">The event has not come yet</div>'}
								<div class="calendar_post__info">Publication Date: <strong>${responce.date_publication_start}</strong></div>
								<div class="calendar_post__excerpt">${responce.body_short}</div>
								<div class="calendar_post__info">${responce.hashtag}</div>
								<div class="calendar_post__info">Website: <strong>${responce.website}</strong></div>
							</div>

						`;
						$(eventContainer).removeClass('shown').empty();
						$(eventContainer).addClass('shown').append(popup);
						$(eventContainer).css({
							opacity: '1',
  							visibility: 'visible',
							top: offset.top,
							left: offset.left,
							zIndex: '999'
						})

					}
				})
			}, function(){
				$(eventContainer).removeClass('shown').empty();
				$(eventContainer).css({
					opacity: '0',
					top: '-1000%',
					left: '-1000%',
					zIndex: '-999',
					visibility: 'hidden'
				})
				return;
			})
			
		}, 1000);	
	});
</script>
