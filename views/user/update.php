<?php

use app\models\ChangeEmailForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserModel */
/* @var $modelForm app\models\UserUpdateForm */
/* @var $loadCoverModelForm app\models\LoadCoverForm */
/* @var $loadPhotoModelForm app\models\LoadPhotoForm */
/* @var $changeEmailForm app\models\ChangeEmailForm */

$this->title = 'Update ' . $model->username;
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="cabinet">
	<div class="page__header" style="background-image: url('<?php echo !empty($model->cover) ? "/" . \app\models\UserModel::FILE_COVER . $model->cover : "/images/member-cover.jpg"; ?>')">
		<div class="page__wrapper cabinet__header">
			<div class="cabinet__avatar cabinet__avatar--on">
				<img src="<?php echo !empty($model->photo) ?"/" . \app\models\UserModel::FILE_PHOTO . $model->photo : "/images/user-avatar.png"; ?>" alt="">
				<div class="cabinet__avatar_edit">
					<i class="cera-icon cera-edit-2"></i>
				</div>
			</div>
			<div class="cabinet__user_info">
				<div class="cabinet__user_name"><?php echo $model->username; ?></div>
				<div class="cabinet__activity">Online</div>
			</div>
			<a href="<?php echo \yii\helpers\Url::toRoute(['/user/update']); ?>" class="btn cabinet__edit_image"><i class="cera-icon cera-edit-2"></i> Edit Cover Image</a>
		</div>
	</div>
	<nav class="cabinet__nav">
		<div class="page__wrapper">
			<ul>
				<li><a class="active" href="<?php echo \yii\helpers\Url::toRoute(['/user/view']); ?>">Profile</a></li>
			</ul>
		</div>
	</nav>

	<div class="cabinet__content">
		<div class="page__wrapper">
			<nav class="cabinet__info_nav">
				<ul class="">
					<li><a  href="<?php echo \yii\helpers\Url::toRoute(['/user/view']); ?>"><i class="cera-icon cera-user"></i> Main profile</a></li>
					<li><a class="active" href="<?php echo \yii\helpers\Url::toRoute(['/user/update']); ?>"><i class="cera-icon cera-edit-2"></i> Edit Profile</a></li>
					<li><a href="<?php echo \yii\helpers\Url::toRoute(['/user-social/index']); ?>"><i class="cera-icon cera-image"></i> Social Networks</a></li>
				</ul>
			</nav>
			<div class="cabinet__inner">
				<div class="user-model-form bg-white">
					<?php $form = ActiveForm::begin(); ?>
						<div class="row">
							<div class="col-lg-6">
								<?= $form->field($modelForm, 'username')->textInput(['maxlength' => true]) ?>
								<?= $form->field($modelForm, 'first_name')->textInput(['maxlength' => true]) ?>
								<?= $form->field($modelForm, 'last_name')->textInput(['maxlength' => true]) ?>
								<?= $form->field($modelForm, 'birthdate', ['inputOptions' => [
									'autocomplete' => 'off']])->widget(DatePicker::class, [
									'type' => DatePicker::TYPE_COMPONENT_PREPEND,
									'pluginOptions' => [
										'autoclose' => true,
										'format' => 'yyyy-mm-dd'
									]
								]) ?>
								<?= $form->field($modelForm, 'gender')->dropDownList(\app\models\UserModel::statusList()) ?>
							</div>
							<div class="col-lg-6">
								<?= $form->field($modelForm, 'oldPassword')->passwordInput(['maxlength' => true]) ?>
								<?= $form->field($modelForm, 'newPassword')->passwordInput(['maxlength' => true]) ?>
								<?= $form->field($modelForm, 'confirmPassword')->passwordInput(['maxlength' => true]) ?>
							</div>
						</div>
						<div class="btn-group">
							<?= Html::submitButton('Save', ['class' => 'btn btn-lg btn-primary']) ?>
						</div>
					<?php ActiveForm::end(); ?>
				</div>
				
				<div class="user-model-form bg-white mt-5">
					<div class="row">
						<div class="col-lg-6">
							<h2>Change email form</h2>
							<p>Your active email is <b><?php echo $model->email; ?></b></p>
							<?php $form = ActiveForm::begin(['action' => ['user/change-email'], 'options' => ['method' => 'post']]); ?>
							<?= $form->field($changeEmailForm, 'email')->textInput(['maxlength' => true]) ?>
							<div class="btn-group">
								<?= Html::submitButton('Change', ['class' => 'btn btn-lg btn-primary']) ?>
							</div>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
				</div>
				
				<div class="user-model-form bg-white mt-5">
					<div class="row">
						<div class="col-lg-6">
							<?php if (empty($model->photo)) { ?>
								<h2 class="mb-5">Change Avatar</h2>
								<?php $loadPhotoForm = ActiveForm::begin(['action' => ['user/update-photo'], 'options' => ['method' => 'post']]); ?>
								
								<?= $loadPhotoForm->field($loadPhotoModelForm, 'photoFile')->widget(FileInput::class); ?>
								
								<?php ActiveForm::end(); ?>
							<?php }else{ ?>
								<a href="<?php echo Url::toRoute(['/user/delete-photo']); ?>" title="Delete"
								   aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?"
								   class="btn btn-warning"
								   data-method="post">Delete User Photo</a>
							<?php }; ?>
						</div>
						<div class="col-lg-6">
							<?php
							if (empty($model->cover)) { ?>
								<h2 class="mb-5">Load Cover Image</h2>
								<?php $loadCoverForm = ActiveForm::begin(['action' => ['user/update-cover'], 'options' => ['method' => 'post']]); ?>
								
								<?= $loadCoverForm->field($loadCoverModelForm, 'coverFile')->widget(FileInput::class); ?>
								
								<?php ActiveForm::end(); ?>
							<?php }else{ ?>
								<a href="<?php echo Url::toRoute(['/user/delete-cover']); ?>" title="Delete"
								   aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?"
								   class="btn btn-warning"
								   data-method="post">Delete User Cover</a>
							<?php }; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
