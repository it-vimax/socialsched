<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserModel */

$this->title = $model->username;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="cabinet">
	<div class="page__header" style="background-image: url('<?php echo !empty($model->cover) ? "/" . \app\models\UserModel::FILE_COVER . $model->cover : "/images/member-cover.jpg"; ?>')">
		<div class="page__wrapper cabinet__header">
			<div class="cabinet__avatar cabinet__avatar--on">
				<img src="<?php echo !empty($model->photo) ?"/" . \app\models\UserModel::FILE_PHOTO . $model->photo : "/images/user-avatar.png"; ?>" alt="">
				<a href="<?php echo \yii\helpers\Url::toRoute(['/user/update']); ?>" class="cabinet__avatar_edit">
					<i class="cera-icon cera-edit-2"></i>
				</a>
			</div>
			<div class="cabinet__user_info">
				<div class="cabinet__user_name"><?php echo $model->username; ?></div>
				<div class="cabinet__activity">Online</div>
			</div>
			<a href="<?php echo \yii\helpers\Url::toRoute(['/user/update']); ?>" class="btn cabinet__edit_image"><i class="cera-icon cera-edit-2"></i> Edit Cover Image</a>
		</div>
	</div>
	<nav class="cabinet__nav">
		<div class="page__wrapper">
			<ul>
				<li><a class="active" href="<?php echo \yii\helpers\Url::toRoute(['/user/view']); ?>">Profile</a></li>
			</ul>
		</div>
	</nav>

	<div class="cabinet__content">
		<div class="page__wrapper">
			<nav class="cabinet__info_nav">
				<ul class="">
					<li><a class="active" href="<?php echo \yii\helpers\Url::toRoute(['/user/view']); ?>"><i class="cera-icon cera-user"></i> Main profile</a></li>
					<li><a href="<?php echo \yii\helpers\Url::toRoute(['/user/update']); ?>"><i class="cera-icon cera-edit-2"></i> Edit Profile</a></li>
					<li><a href="<?php echo \yii\helpers\Url::toRoute(['/user-social/index']); ?>"><i class="cera-icon cera-image"></i> Social Networks</a></li>
				</ul>
			</nav>
			<div class="cabinet__inner">
				<div class="cabinet__table">
					<div class="cabinet__table_row">
						<div class="cabinet__table_item">Name</div>
						<div class="cabinet__table_item"><?php echo $model->username; ?></div>
					</div>
					<div class="cabinet__table_row">
						<div class="cabinet__table_item">First Name</div>
						<div class="cabinet__table_item"><?php echo $model->first_name; ?></div>
					</div>
					<div class="cabinet__table_row">
						<div class="cabinet__table_item">Last Name</div>
						<div class="cabinet__table_item"><?php echo $model->last_name; ?></div>
					</div>
					<div class="cabinet__table_row">
						<div class="cabinet__table_item">email</div>
						<div class="cabinet__table_item"><?php echo $model->email; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
