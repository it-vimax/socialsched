<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Url;

/**
 * @var $models \app\models\Memberships[]
 */
/** @var \app\models\UserModel $user */
$user = \app\models\User::findOne(Yii::$app->getUser()->getId())
?>

<div class="page">
	<div class="page__wrapper py-5">
		<h4 class="page__subtitle">Membership Levels</h4>
		<h2 class="page__title">Membership Levels</h2>

		<div class="membership">
			<div class="row">
				<?php
				/** @var \app\models\Memberships $membership */
				foreach ($models as $membership) { ?>
					<div class="membership__item col-md-6 col-lg <?= (!empty($user->getMembership()->one()) && $user->getMembership()->one()->id === $membership->id) ? 'active' : ''?>">
						<div class="card h-100 ">
							<div class="card-body">
								<h2 class="membership__title">
									<strong> <?php echo $membership->name; ?> </strong>
								</h2>
								<div class="membership__price">
									<strong><?= $membership->price && $membership->price != 0 ? '$ '. $membership->price .'' : 'Free'?> </strong>
								</div>
								<div class="membership__description"> <?php echo BaseStringHelper::truncate($membership->description, 100); ?> </div>
							</div>
							<div class="card-footer">
								<?php if (!empty($user->getMembership()->one()) && $user->getMembership()->one()->id === $membership->id) { ?>
									<a class="btn btn-lg btn-primary w-100"
									   href="<?php echo Url::toRoute(['pricing-plans/buy', 'id' => $membership->id]); ?>"
									   title="<?php echo $membership->name; ?>"
									   data-method="post">Your Level</a>
								<?php } else { ?>
									<a class="btn btn-lg btn-primary w-100"
									   href="<?php echo Url::toRoute(['pricing-plans/buy', 'id' => $membership->id]); ?>"
									   title="<?php echo $membership->name; ?>"
									   data-method="post">Select</a>
								<?php }; ?>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
