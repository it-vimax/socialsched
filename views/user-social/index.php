<?php

/**
 * @var $otherSocials \app\models\Socials[]
 * @var $userSocials \app\models\Socials[]
 */

use yii\helpers\Url;

?>

<div class="page">
	<div class="page__wrapper py-5">
		<h4 class="page__subtitle">Connected Socials</h4>
		<div class="socials">
			<h3 class="block__title">Connected Socials</h3>
			<div class="socials__connected">
				<?php
				/** @var \app\models\Socials $userSocial */
				foreach ($userSocials as $userSocial) { ?>
					<?php if(empty( $userSocial->logo)) {?>
						<a class="fa fa-<?php echo $userSocial->name; ?> socials__item" href="<?php echo Url::toRoute(['/user-social/detach-social', 'id' => $userSocial->id]); ?>" data-method="post"></a>
					<?php } else { ?>
						<a class="socials__item" href="<?php echo Url::toRoute(['/user-social/detach-social', 'id' => $userSocial->id]); ?>" data-method="post">
							<img src="/<?= \app\models\Socials::FILE_PATH . $userSocial->logo ?>">
						</a>
					<?php }; ?>
				<?php }; ?>
			</div>
			<hr>
			<h3 class="block__title">You may connect other socials</h3>
			<div class="socials__disconnected">
				<?php
				/** @var \app\models\Socials $otherSocial */
				foreach ($otherSocials as $otherSocial) { ?>
					<?php if(empty( $otherSocial->logo)) {?>
						<a class="fa fa-<?php echo $otherSocial->name; ?> socials__item"
						href="<?php echo Url::toRoute(['/user-social/attach-social', 'id' => $otherSocial->id]); ?>"
						data-method="post"></a>
					<?php } else { ?>
						<a class="socials__item" href="<?php echo Url::toRoute(['/user-social/attach-social', 'id' => $otherSocial->id]); ?>" data-method="post">
							<img src="/<?= \app\models\Socials::FILE_PATH . $otherSocial->logo ?>">
						</a>
					<?php }; ?>
					
				<?php }; ?>
			</div>
		</div>
	</div>
</div>
