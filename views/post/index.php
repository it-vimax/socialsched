<?php

use yii\helpers\BaseStringHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $models \app\models\Post[] */

$this->title = 'Post list';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="page">
	<div class="page__wrapper py-5">
		<h4 class="page__subtitle">Post list</h4>
		<h2 class="page__title">Post list</h2>

		<div class="posts row">
			
			<?php foreach ($models as $model): ?>

				<div class="col-lg-4 col-md-6 post">
					<article class="post__item">
						<?php if (!empty($model->media)) { ?>
							<div class="post__preview">
								<a href="<?php echo Url::toRoute(['/post/view', 'id' => $model->id]); ?>"><img src="/<?php echo \app\models\Post::FILE_PATH . $model->media; ?>" alt="<?php echo $model->title; ?>"></a>
							</div>
						<?php }; ?>
						
						<div class="post__content">
							<h2 class="post__title"><a href="<?php echo Url::toRoute(['/post/view', 'id' => $model->id]); ?>"><?php echo $model->title; ?></a></h2>
							<div class="post_inner__publish">Publication date: <strong><?php echo $model->date_publication_start; ?></strong></div>
							<div class="post_inner__socials">
								Publicate to: 
								<?php
								/** @var \app\models\Socials $social */
								foreach ($model->getSocials()->all() as $social) {; ?>
									<strong><?php echo $social->name; ?></strong>
								<?php }; ?>
							</div>
							<div class="post__excerpt"><?php echo BaseStringHelper::truncate($model->body, 100); ?></div>
							<a class="post__link" href="<?php echo Url::toRoute(['/post/view', 'id' => $model->id]); ?>">Continue reading</a>
						</div>
					</article>
				</div>
			<?php endforeach; ?>
		</div>
<!--		<div class="pagination">-->
<!--			<ul class="pagination__list">-->
<!--				<li class="pagination__item"><a class="pagination__link" href="#">&#x276E;</a></li>-->
<!--				<li class="pagination__item"><a class="pagination__link" href="#">1</a></li>-->
<!--				<li class="pagination__item"><a class="pagination__link" href="#">2</a></li>-->
<!--				<li class="pagination__item"><a class="pagination__link" href="#">3</a></li>-->
<!--				<li class="pagination__item"><a class="pagination__link" href="#">&#x276F;</a></li>-->
<!--			</ul>-->
<!--		</div>-->
	</div>
</div>
