<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="post-view page">

	<div class="page__wrapper py-5">
   	
		<div class="post_inner">
			<div class="post_inner__content">
				<?php if (!empty($model->media)) { ?>
					<img src="<?php echo $model->getPath(); ?>" alt="<?php echo $model->title; ?>">
				<?php }; ?>
				<h2 class="post_inner__title"><?php echo $model->title; ?></h2>
				<div class="post_inner__staus">
					<div class="badge badge-pill badge-secondary">
						<?php
							if ($model->isPublished()) {
								echo '<span>This post has been published.</span>';
							} else {
								echo '<span>This post has not yet been published</span>';
							}
						?>
					</div>
				</div>
				<div class="post_inner__text"><?php echo $model->body; ?></div>
				<div class="post_inner__publish">Publication date: <strong><?php echo $model->date_publication_start; ?></strong></div>
				<?= $model->website ? '<p>Website ' . $model->website . ' </p>' : ''; ?>
				<!-- <p>Status <?= $model->status; ?></p> -->
				<div class="post_inner__socials">
					Publicate to: 
					<?php
					/** @var \app\models\Socials $social */
					foreach ($model->getSocials()->all() as $social) {; ?>
						<strong><?php echo $social->name; ?></strong>
					<?php }; ?>
				</div>

				<div class="post_inner__edit_post">
					<a href="<?php echo \yii\helpers\Url::toRoute(['/post/update', 'id' => $model->id]); ?>" class="btn btn-lg btn-primary">Edit post</a>
				</div>
			</div>
		</div>
	</div>

</div>
