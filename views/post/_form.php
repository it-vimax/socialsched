<?php

use app\models\Post;
use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostForm */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://cdn.tiny.cloud/1/zxmp5lpicnhmeex0y9h60r7btfx12ooizxhra6jm4he1g3od/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
		<div class="col-lg-6">
			<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'body')->textarea(['rows' => '6', 'id' => 'mytextarea']) ?>
			
			<div class="row">
				<div class="col-lg-6"><?= $form->field($model, 'hashtag')->textInput(['maxlength' => true]) ?></div>
				<div class="col-lg-6"><?= $form->field($model, 'website')->textInput(['maxlength' => true]) ?></div>
				<div class="col-lg-6"><?= $form->field($model, 'status')->dropDownList(Post::statusList()) ?></div>
				<div class="col-lg-6">
					<?= $form->field($model, 'date_publication_start', ['inputOptions' => [
						'autocomplete' => 'off']])->widget(DateTimePicker::class, [
						'name' => 'birthdate',
						'options' => ['placeholder' => 'Select date publication ...'],
						'convertFormat' => true,
						'pluginOptions' => [
							'format' => 'yyyy-MM-dd HH:mm:ss',
							'startDate' => date('Y-m-d H:i:s'),
							'todayHighlight' => true
						]
					]) ?>	
				</div>
			</div>
		</div>
	
		
		<div class="col-lg-6">
			<?= $form->field($model, 'mediaFile')->widget(FileInput::classname(), [
				'options' => [/*'multiple' => true,*/ 'accept' => 'image/*'],
			]); ?>
		</div>
	</div>
	
	<?php $socials = \app\models\Socials::userSocialsList(Yii::$app->user->id);
	if (empty($socials)) { ?>
		<div class="alert">You need to register in at least one social network.</div>
		<div class="form-group">
			<?= Html::submitButton('Save draft', ['class' => 'btn btn-lg btn-secondary', 'disabled' => true]) ?>
		</div>
	<?php }else{ ?>
		<div class="form-group social_check">
			<?php echo $form->field($model, 'socials[]')->checkboxList(\app\models\Socials::userSocialsList(Yii::$app->user->id)); ?>
		</div>
		<div class="btn-group">
			<?= Html::submitButton('Save draft', ['class' => 'btn btn-lg btn-primary', 'id' => 'save_post_btn']) ?>
		</div>
	<?php }; ?>

    <?php ActiveForm::end(); ?>

</div>
<script>
	document.addEventListener("DOMContentLoaded", function(event) {
		tinymce.init({
			selector: "#mytextarea",
			plugins: "emoticons",
			toolbar: "emoticons",
			toolbar_location: "bottom",
			menubar: false
		});

		let selectedValue = $('#postform-status').val();
		function statusPost(selectedValue){
			switch (selectedValue) {
				case 'on':
					$('#save_post_btn').text('Schedule');
					break;
				default:
					$('#save_post_btn').text('Save as draft');
					break;
			}
		};
		statusPost(selectedValue);
		$('#postform-status').on('change', function(){
			statusPost($(this).val());
		});
	});
</script>