<?php

use kartik\datetime\DateTimePicker;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $modelForm app\models\PostUpdateForm */
/* @var $postUpdateMediaForm app\models\PostUpdateMediaForm */

$this->title = 'Update Post: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<script src="https://cdn.tiny.cloud/1/zxmp5lpicnhmeex0y9h60r7btfx12ooizxhra6jm4he1g3od/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<div class="post-update page">

    <div class="page__wrapper py-5">
    	<h1 class="page__title mb-5"><?= Html::encode($this->title) ?></h1>
		
		<div class="post-form">
			<div class="row">
				<div class="col-lg-6">
					<?php $form = ActiveForm::begin(); ?>
					
					<?= $form->field($modelForm, 'title')->textInput(['maxlength' => true]) ?>
					
					<?= $form->field($modelForm, 'body')->textarea(['rows' => 6, 'id' => 'mytextarea']) ?>
		
					<div class="row">
						
						<div class="col-lg-6"><?= $form->field($modelForm, 'hashtag')->textInput(['maxlength' => true]) ?></div>
						
						<div class="col-lg-6"><?= $form->field($modelForm, 'website')->textInput(['maxlength' => true]) ?></div>
						
					</div>
					<div class="row">
						
						<div class="col-lg-6"><?= $form->field($modelForm, 'status')->dropDownList(\app\models\Post::statusList()) ?></div>
						
						<div class="col-lg-6">
							<?= $form->field($modelForm, 'date_publication_start', ['inputOptions' => [
								'autocomplete' => 'off']])->widget(DateTimePicker::class, [
								'name' => 'date_publication_start',
								'options' => ['placeholder' => 'Select date publication ...'],
								'convertFormat' => true,
								'pluginOptions' => [
									'format' => 'yyyy-MM-dd HH:mm:ss',
									'startDate' => date('Y-m-d H:i:s'),
									'todayHighlight' => true
								]
							]) ?>
						</div>
						
					</div>
					
					<?= $form->field($modelForm, 'socials[]')->checkboxList(\app\models\Socials::userSocialsList(Yii::$app->user->id), [
						'item' => function($index, $label, $name, $checked, $value) use ($model){
							$postSocialsArr = $model->getSocialsArray();
							if(array_key_exists($value, $postSocialsArr)){
								$checked = 'checked';
							}
							return Html::checkbox($name, $checked, [
								'value' => $value,
								'label' => $label,
							]);
						}
					]) ?>
			
					<div class="form-group">
						<?= Html::submitButton('Update', ['class' => 'btn btn-lg btn-primary', 'id' => 'save_post_btn']) ?>
					</div>
					
					<?php ActiveForm::end(); ?>
				</div>
	
				<div class="col-lg-6">
					<?php if (empty($model->media)) { ?>
						<?php $form2 = ActiveForm::begin(['action' => ['post/update-media-file', 'id' => $model->id], 'options' => ['method' => 'post', 'class' => 'file_upload__form']]); ?>
						
							<?= $form2->field($postUpdateMediaForm, 'mediaFile')->widget(FileInput::classname(), [
								'options' => ['accept' => 'image/*'],
							]); ?>
						<?php ActiveForm::end(); ?>
					<?php } else { ?>
						<div class="post_edit__image">
							<img src="<?php echo $model->getPath(); ?>" alt="<?php echo $model->title; ?>">
							<a href="<?php echo Url::toRoute(['/post/delete-media-file', 'id' => $model->id]); ?>" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this item?" class="btn btn-danger" data-method="post">Delete Media File</a>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>

		
    </div>
</div>


<script>
	document.addEventListener("DOMContentLoaded", function(event) {
		tinymce.init({
			selector: "#mytextarea",
			plugins: "emoticons",
			toolbar: "emoticons",
			toolbar_location: "bottom",
			menubar: false
		});

		let selectedValue = $('#postupdateform-status').val();
		function statusPost(selectedValue){
			switch (selectedValue) {
				case 'on':
					$('#save_post_btn').text('Schedule');
					break;
				default:
					$('#save_post_btn').text('Save as draft');
					break;
			}
		};
		statusPost(selectedValue);
		$('#postupdateform-status').on('change', function(){
			statusPost($(this).val());
		});
	});
</script>