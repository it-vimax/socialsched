<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Socials */

$this->title = 'Update Social connection: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Socials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="socials-form bg-white">
	
	<?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'logo')->textInput(); ?>

	<div class="btn-group">
		<?= Html::submitButton('Save social', ['class' => 'btn btn-lg btn-primary']) ?>
	</div>
	
	<?php ActiveForm::end(); ?>

</div>