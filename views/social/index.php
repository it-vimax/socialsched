<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Socials';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="socials-index page">
    <div class="page__wrapper py-5">
        <div class="d-flex align-items-center justify-content-between mb-5">
            <h1 class="page__title"><?= Html::encode($this->title) ?></h1>
            <div><?= Html::a('Create Socials', ['create'], ['class' => 'btn btn-lg btn-primary']) ?></div>
        </div>
        <div class="socials__table">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'name',
                    'url:url',
					[
						'attribute' => 'logo',
						'format'=>'html',
						'value' => function ($model) {
							return !empty($model->logo) ? '<img class="social__preview" src="/' . \app\models\Socials::FILE_PATH . $model->logo . '">' : '';
						},
					],
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>

    </div> 

</div>
