<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SocialForm */

$this->title = 'Create Social connection';
$this->params['breadcrumbs'][] = ['label' => 'Socials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="socials-create page">
    <div class="page__wrapper py-5">
        <h1 class="page__title mb-5"><?= Html::encode($this->title) ?></h1>
    
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
