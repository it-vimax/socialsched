<?php
/**
 * @var $userModel \app\models\UserModel
 * @var $model \app\models\UserHelpMessageForm
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="page">
	<div class="page__wrapper py-5">
		<h4 class="page__subtitle">Help</h4>
		<h2 class="page__title">Help</h2>
		<div class="help__form">
			<?php $form = ActiveForm::begin([
				'options' => [
					'class' => 'form-help',
				],
				'id' => 'login-form',
				'fieldConfig' => [
					'template' => "<div class=\"form-label-group mb-5\">{label}{input}</div>\n<div class=\"input-error-message\">{error}</div>",
					'labelOptions' => ['class' => ''],
				],
			]); ?>
				<?= $form->field($model, 'name')->textInput(['autofocus' => true, 'class' => 'form-control bg-white']) ?>
				<?= $form->field($model, 'email')->textInput(['autofocus' => true, 'class' => 'form-control bg-white']) ?>
				<?= $form->field($model, 'subject')->textInput(['autofocus' => true, 'class' => 'form-control bg-white']) ?>
				<?= $form->field($model, 'body')->textarea(['autofocus' => true, 'class' => 'form-control bg-white textarea']) ?>
				<div class="btns-group">
					<?= Html::submitButton('Submit', ['class' => 'btn btn-lg btn-primary',]) ?>
				</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
