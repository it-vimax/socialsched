<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Memberships';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="memberships-index page">

    <div class="page__wrapper py-5">
        <div class="d-flex align-items-center justify-content-between mb-5">
            <h1 class="page__title"><?= Html::encode($this->title) ?></h1>
            <div><?= Html::a('Create Memberships', ['create'], ['class' => 'btn btn-lg btn-primary']) ?></div>
        </div>

        <div class="socials__table">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    'name',
                    'description:ntext',
                    'level',
                    'platforms_count',
                    'publications_count',
                    'price',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
