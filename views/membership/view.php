<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Memberships */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Memberships', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="memberships-view page">

    <div class="page__wrapper py-5">
        <div class="d-flex align-items-center justify-content-between mb-5">
            <h1 class="page__title"><?= Html::encode($this->title) ?></h1>
            <div>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-lg btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-lg btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="socials__table">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'description:ntext',
                    'level',
                    'platforms_count',
                    'publications_count',
                    'price',
                ],
            ]) ?>
        </div>

    </div>
</div>
