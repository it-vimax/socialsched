<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Memberships */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="memberships-form bg-white">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'level')->textInput() ?>

        <?= $form->field($model, 'platforms_count')->textInput() ?>

        <?= $form->field($model, 'publications_count')->textInput() ?>

        <?= $form->field($model, 'price')->textInput() ?>

        <div class="btn-group">
            <?= Html::submitButton('Save plan', ['class' => 'btn btn-lg btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
