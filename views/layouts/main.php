<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Alert;
use yii\helpers\Html;

\app\assets\MainAsset::register($this);

/** @var \app\models\UserModel $user */
$user = \app\models\UserModel::findOne(Yii::$app->user->identity->id);

$menuItems = [];
if (Yii::$app->getUser()->getId() === 1) {
	$menuItems[] = ['label' => 'Social', 'url' => '/social/index', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'social/index'];
	$menuItems[] = ['label' => 'Membership', 'url' => '/membership/index', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'membership/index'];
	$menuItems[] = ['label' => 'Help Messages', 'url' => '/help-messages/index', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'help-messages/index'];
}

if (Yii::$app->user->isGuest) {
	$menuItems[] = ['label' => 'Lending', 'url' => '/site/index', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'site/calendar'];
	$menuItems[] = ['label' => 'Signup', 'url' => '/site/signup', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'site/signup'];
	$menuItems[] = ['label' => 'Login', 'url' => '/site/login', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'site/login'];
} else {
	$menuItems[] = ['label' => 'Dashboard', 'url' => '/site/calendar', 'icon' => 'cera-icon cera-layout', 'isActive' =>  $this->context->route == 'site/calendar'];
	$menuItems[] = ['label' => 'Connected Socials', 'url' => '/user-social/index', 'icon' => 'cera-icon cera-heart', 'isActive' =>  $this->context->route == 'user-social/index'];
	$menuItems[] = ['label' => 'Upcoming Posts (List)', 'url' => '/post/index', 'icon' => 'cera-icon cera-list', 'isActive' =>  $this->context->route == 'post/index'];
	$menuItems[] = ['label' => 'Schedule New Post', 'url' => '/post/create', 'icon' => 'cera-icon cera-file', 'isActive' =>  $this->context->route == 'post/create'];
	$menuItems[] = ['html' => '<li class="sidebar__nav_item"><span class="sidebar__nav_item_devider"><span>Membership</span></span></li>'];
	$menuItems[] = ['label' => 'Pricing plans', 'url' => '/pricing-plans/index', 'icon' => 'cera-icon cera-dollar-sign', 'isActive' =>  $this->context->route == 'pricing-plans/index'];
	$menuItems[] = ['html' => '<li class="sidebar__nav_item"><span class="sidebar__nav_item_devider"><span>Others</span></span></li>'];
	$menuItems[] = ['label' => 'Help', 'url' => '/user-help-message/create', 'icon' => 'cera-icon cera-life-buoy', 'isActive' =>  $this->context->route == 'user-help-message/create'];
	$menuItems[] = ['label' => 'Log out', 'url' => '/site/logout', 'icon' => 'cera-icon cera-log-out', 'isActive' =>  $this->context->route == 'site/logout'];
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="index, follow">
	<meta name="google" content="notranslate">
	<meta name="format-detection" content="telephone=no">
	<meta name="description" content="">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="site-container">
	<aside class="sidebar">
		<div class="sidebar__header">
			<div class="sidebar__logo">
				<a href="/post/create">New post <i class="fa fa-plus"></i></a>
			</div>
			<button type="button" class="btn btn-link">
				<i class="cera-icon cera-menu-arrow"></i>
			</button>
		</div>
		<nav class="sidebar__nav">
			<ul class="sidebar__nav_list">
				<?php foreach ($menuItems as $menuItem) { ?>
					<?php if (key_exists('html', $menuItem)) {
						echo $menuItem['html'];
					}else{; ?>
						<li class="sidebar__nav_item">
							<a class="sidebar__nav_item_link <?php echo !empty($menuItem['isActive']) ? 'active' : ''; ?>" href="<?php echo $menuItem['url']; ?>">
								<i class="<?php echo $menuItem['icon']; ?>"></i> <span><?php echo $menuItem['label']; ?></span>
							</a>
						</li>
					<?php }; ?>
				<?php }; ?>
			</ul>
		</nav>
	</aside>
	
	<main class="site-content">
		<header class="header">
			<div class="profile">
				<div class="profile__visible">
					<div class="profile__avatar">
						<img src="<?php echo !empty($user->photo) ? '/' . \app\models\UserModel::FILE_PHOTO . $user->photo : '/images/user-avatar.png'; ?>" alt="<?php echo $user->username; ?>">
					</div>
					<div class="profile__name"><?php echo $user->username; ?></div>
				</div>
				<div class="profile__dropdown">
					<ul class="profile__nav">
						<li class="profile__item"><a href="<?php echo \yii\helpers\Url::toRoute(['user/view']); ?>"><i class="cera-icon cera-user"></i> Profile</a></li>
						<li class="profile__item"><a href="<?php echo \yii\helpers\Url::toRoute(['user/update']); ?>"><i class="cera-icon cera-settings"></i> Settings</a></li>
						<li class="profile__item"><a href="<?php echo \yii\helpers\Url::toRoute(['site/logout']); ?>"><i class="cera-icon cera-log-out"></i> Log out</a></li>
					</ul>
				</div>
			</div>
		</header>

		<div class="content">
			<?= $content ?>
		</div>

		<footer class="">
			<div class="row d-flex flex-row justify-content-between align-items-center">
				<p class="m-0 col-md-6">Copyright © 2019 Cera. All rights reserved</p>
				<ul class="col-md-6">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-slack"></i></a></li>
				</ul>
			</div>
		</footer>
	</main>
</div>

<?php $this->endBody() ?>
<script>
	if($(window).width() < 768){
		$('.sidebar').addClass('closed')
	}
	$(window).resize(function(){
		if($(this).width() < 768){
			$('.sidebar').addClass('closed')
		}
	})
</script>
</body>
</html>
<?php $this->endPage() ?>
